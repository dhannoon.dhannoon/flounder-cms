<?php
require(__DIR__."/classes/Createcourse.class.php");
require(__DIR__."/classes/Createusers.class.php");
require(__DIR__."/classes/Database.class.php");
require(__DIR__."/classes/Login.class.php");
require(__DIR__."/classes/Profile.class.php");
require(__DIR__."/classes/Request.class.php");
require(__DIR__."/classes/Sessions.class.php");
require(__DIR__."/classes/Useredit.class.php");
require(__DIR__."/classes/Validation.class.php");
Session::init();
# make a connection to the database
//require("css/main.css");

require (__DIR__."/pages/template/header.php");

if(!empty($_GET["p"])) {
    if(file_exists(__DIR__."/pages/".$_GET["p"].".php")) {
            require (__DIR__."/pages/".$_GET["p"].".php");
    } else {
        require (__DIR__."/pages/404.php");
        echo "<br>";
        echo "404";
        die();
    }
}else {
    require (__DIR__."/pages/home.php");
}

require (__DIR__."/pages/template/footer.php");

?>