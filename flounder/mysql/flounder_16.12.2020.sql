-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2020 at 11:43 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flounder`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `course_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_start` datetime DEFAULT NULL,
  `course_end` datetime DEFAULT NULL,
  `exam_date` datetime DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_html` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `userid`, `course_name`, `course_start`, `course_end`, `exam_date`, `note`, `document`, `course_html`) VALUES
(1, 14, 'englishh', '1212-12-12 00:00:00', '1212-12-12 00:00:00', '1212-12-12 00:00:00', NULL, NULL, NULL),
(2, 14, 'deutsch', '2020-12-12 00:00:00', '2020-12-20 00:00:00', '2020-01-10 00:00:00', '10', NULL, '<p style=\"text-align: center;\">wegawerg</p><p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\"><br></p><ol><li style=\"text-align: left;\">wefwf12312wdwdw</li><li style=\"text-align: left;\">12e</li><li style=\"text-align: left;\">12</li><li style=\"text-align: left;\">e12</li><li style=\"text-align: left;\">e</li></ol>'),
(3, 14, '12', '1212-12-12 00:00:00', '1221-12-12 00:00:00', '1221-12-12 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course_material`
--

CREATE TABLE `course_material` (
  `id` int(11) NOT NULL,
  `course_material_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `course_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_material_to_course`
--

CREATE TABLE `course_material_to_course` (
  `course_id` int(11) NOT NULL,
  `course_material_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_participants`
--

CREATE TABLE `course_participants` (
  `user_id` int(255) NOT NULL,
  `course_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_participants`
--

INSERT INTO `course_participants` (`user_id`, `course_id`) VALUES
(14, 2);

-- --------------------------------------------------------

--
-- Table structure for table `role_type`
--

CREATE TABLE `role_type` (
  `id` int(255) NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_type`
--

INSERT INTO `role_type` (`id`, `role_name`) VALUES
(1, 'admin'),
(2, 'teacher'),
(3, 'student');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT current_timestamp(),
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_type_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_type_id` int(255) DEFAULT NULL,
  `is_account_confirmed` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `date_of_birth`, `gender`, `profile_image`, `created_at`, `updated_at`, `last_seen`, `status`, `address`, `country`, `city`, `zip`, `phone`, `role_type_name`, `role_type_id`, `is_account_confirmed`) VALUES
(14, 'wef', 'wef', 'dhannoon_dhannoon@icloud.com', '$2y$10$klDXRVK2CyR.X12.Upge6ur2xZXKbz9XD1OoQ.THjyjx8.FaO/WFG', '0000-00-00', NULL, NULL, '2020-12-12 12:40:09', NULL, '2020-12-12 12:40:09', NULL, NULL, NULL, NULL, NULL, 'wefwef', NULL, 1, NULL),
(15, 'we', 'we', 'dhannoon_dhannoon1@icloud.com', '$2y$10$yeLnXndzZC7raOechwLUGujbkvBseLzENXks6p9ROtibc9FlTFapy', '1212-12-12', NULL, NULL, '2020-12-12 13:20:16', NULL, '2020-12-12 13:20:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '12', '21', 'dhannoon_dhanno21on@icloud.com', '$2y$10$QBt5sMfTTDz33TYLV0YwSeQPazcz4Gdl2EMV7Xij6rJGK68r4P64C', '1212-03-12', NULL, NULL, '2020-12-12 13:22:13', NULL, '2020-12-12 13:22:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '1212', '1212', 'dhannoon_d12hannoon@icloud.com', '$2y$10$Lw6SnJpzW8fa3EF8R70eTuFeSHKEDaSzzvCKOGSRhUoSj5Nw1eVtO', '1212-12-12', NULL, NULL, '2020-12-12 13:23:27', NULL, '2020-12-12 13:23:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'qwdqwd', 'qwdqwd', 'dhannoon_dwhannoon@icloud.coqm', '$2y$10$rjDws652FhxWOGT3Kkf5tOX.Z2KgZ1whlODo6YrQe7iUibIPuC1QC', '0000-00-00', NULL, NULL, '2020-12-12 13:25:17', NULL, '2020-12-12 13:25:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'qwdqwd', 'qwdqwd', 'dhannoon_dhannwqoon@icloud.com', '$2y$10$knQAIG.JRltBD98gtnb6jOdfbmpLwcMUN.xatpyBBYTnWqFMHFgsm', '1212-12-21', NULL, NULL, '2020-12-12 13:28:10', NULL, '2020-12-12 13:28:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'haha', 'haha', 'haha@icloud.com', '$2y$10$bHUouBkPbKVd3cd95x5SAOq15HWH4DZAedlo.cuSgU7bytR8OmU8S', '1212-12-12', NULL, NULL, '2020-12-14 09:58:33', NULL, '2020-12-14 09:58:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'header', 'hease', 'dhannoon_^1dhannoon@icloud.com', '$2y$10$wo0fqru0Em7wYt2gcN3Cq.USbe2fzDZZ7yKChQuO/QOz4g.rvOkZW', '0000-00-00', NULL, NULL, '2020-12-14 10:18:56', NULL, '2020-12-14 10:18:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'q212e', '12e', 'd2hannoon_dhannoon@icloud.co2', '$2y$10$TRv6C1wAzy/n8i0xJDIX3eqSbTY2k8VeJ1.5naRRkQY6JHccu0dBi', '1212-12-12', NULL, NULL, '2020-12-14 10:23:16', NULL, '2020-12-14 10:23:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '12', '12', 'dhannoon_dha41noon@icloud.com', '$2y$10$j9wY4LojrvWukaW2b7GClOJtenEkgEcIjctnCEdN.hIhG3sKQUY9C', '1212-02-21', NULL, NULL, '2020-12-14 10:41:52', NULL, '2020-12-14 10:41:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'test', 'test', 'dhannoon_dhantenoon@icloud.com', '$2y$10$YAV4S8fFpLxW7VJUdMC5weLd6giz7gKlZXNbz9d/Csf1XQzxPh.le', '1212-12-12', NULL, NULL, '2020-12-14 11:10:57', NULL, '2020-12-14 11:10:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'test', 'test', 'test@icloud.com', '$2y$10$Jnh2Yd1/alI5XIYHLVyT7uosMmknjm1zDGdE3TYjFFDimcA3Qvzuy', '1212-12-12', NULL, NULL, '2020-12-14 18:42:01', NULL, '2020-12-14 18:42:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_material`
--
ALTER TABLE `course_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_material_to_course`
--
ALTER TABLE `course_material_to_course`
  ADD PRIMARY KEY (`course_id`,`course_material_id`),
  ADD KEY `course_material_id` (`course_material_id`);

--
-- Indexes for table `course_participants`
--
ALTER TABLE `course_participants`
  ADD PRIMARY KEY (`user_id`,`course_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `role_type`
--
ALTER TABLE `role_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_type_id` (`role_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `course_material`
--
ALTER TABLE `course_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_type`
--
ALTER TABLE `role_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_material_to_course`
--
ALTER TABLE `course_material_to_course`
  ADD CONSTRAINT `course_material_to_course_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_material_to_course_ibfk_2` FOREIGN KEY (`course_material_id`) REFERENCES `course_material` (`id`);

--
-- Constraints for table `course_participants`
--
ALTER TABLE `course_participants`
  ADD CONSTRAINT `course_participants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_participants_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
