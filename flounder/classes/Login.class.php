<?php
/**
 * This class has the login function to get the session id for the user
*/

class Login {

    //In this function I will get the user session
    public static function get_user_session($email, $password) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM users WHERE email = ?";
        $statement = $database->prepare($sql);
        $statement->bind_param("s", $email);
        $result = $statement->execute();
        $result = $statement->get_result();
        $user = $result->fetch_assoc();
        if($user === null) {
            echo '<script> alert("Email is not registered!")</script>';
            return false;
            //For more sec.
            Session::destroy();
            die();
        }
        if($user !== null && password_verify($password,  $user["password"])) {
            //Give session user all the information what var user return
            $_SESSION["user"] = $user;
        } else {
            echo '<script> alert("Login did not work maybe the password was not correct!")</script>';
            return false;
            //For more sec.
            Session::destroy();
            die();
        }
        return $_SESSION["user"];
    }
}
?>