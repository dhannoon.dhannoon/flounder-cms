<?php
/**
 * This class session has the the functions to start, destroy the session and get too.
*/

class Session {

    # start the sesstion!
    public static function init() {
        if(session_id() == '') {
            session_start();
        }
    }

    # sets a specific value to a specific key of the session
    public static function set($key, $value) {
        $_SESSION[$key] = $value;
    }


    public static function get($key) {
        if(isset($_SESSION[$key])) {
            $value = $_SESSION[$key];
            return($value);
        }
    }

    #deletes the session for example when you logout!
    public static function destroy() {
        session_destroy();
    }


}

?>