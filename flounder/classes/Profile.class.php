<?php
/**
 * This Class has the functions to manage the profile for all the users
*/

class Profile {

    //This function will select everything from the table users by the session id
    public static function select_everything_from_user($user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_user_information = "SELECT * FROM users WHERE id = ?";
        $statement = $database->prepare($sql_select_user_information);
        $statement->bind_param("i", $user_session_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        $user = $result->fetch_assoc();
        return $user;
    }

    //This function will change the user information
    public static function update_user_information($firstname_update, $lastname_update, $date_of_birth_update, $phone_update, $country_update, $user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_update_user_information = "UPDATE users SET firstname = ?, lastname = ?, date_of_birth = ?, phone = ?, country = ?  WHERE id = ?";
        $statement = $database->prepare($sql_update_user_information);
        $statement->bind_param("sssssi", $firstname_update, $lastname_update, $date_of_birth_update, $phone_update, $country_update, $user_session_id);
        $statement->execute();
    }

    //This function will update the user image
    public static function update_user_image($image_name, $user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_update_user_information = "UPDATE users SET profile_image = ?  WHERE id = ?";
        $statement = $database->prepare($sql_update_user_information);
        $statement->bind_param("si", $image_name, $user_session_id);
        $statement->execute();
    }

    //This function will add the image into user_session_id
    public static function upload_an_image_and_save_into_user_session_id($files_name, $files_tmp_name, $image_users_path, $everything_from_user, $user_session_id) {
        //$files_name shoud be like that: $_FILES["uploadimage_update"]["name"]
        //$image_users_path shoud be like that: __DIR__."/../image/image_users/";
        //$files_tmp_name should be like that $_FILES["uploadimage_update"]["tmp_name"]
        if(!empty($files_name)) {
            //File upload directory
            $image_users = $image_users_path;
            $file_basename = $files_name;
            $filename = basename($file_basename);
            $targetFilePath = $image_users . $filename;
            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
            
            //Allow certain file formats
            $allowTypes = array("jpg", "png", "jpeg", "gif", "pdf");
            
            //Check if the filetype ending in the allowtypes variable!
            if(in_array($fileType, $allowTypes)) {
                //Upload file to server
                if(move_uploaded_file($files_tmp_name, $targetFilePath)) {
                    //Check if the uploaded image not the same image and remove!
                    if($everything_from_user["profile_image"] !== $file_basename) {
                        unlink($image_users . $everything_from_user["profile_image"]);
                    }
                    //Insert image file name into database
                    $update_image = Profile::update_user_image($filename, $user_session_id);
                    //Check if the update image did work without problem!
                    if($update_image !== null) {
                        echo '<script> alert("Updating the image did not work!."); window.location = window.location.href;</script>';
                     } else {
                        echo '<script> window.location = window.location.href; </script>';
                     }
                } else {
                   echo '<script> alert("Moving the file did not work!"); window.location = window.location.href; </script>';
                }
            } else {
                echo '<script> alert("Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload."); window.location = window.location.href; </script>';
            }
        }
    }

    //This part of check password will ask the database if the entered password is verify!
    public function check_entered_password_with_user_password($password, $user_session_id) {
        $error = false;
        //This funtion will return for me the user data!
        $user = $this->select_everything_from_user($user_session_id);
        if($user !== null && password_verify($password,  $user["password"])) {
            $error = false;
        } else {
            $error = true;
        }
        return $error;
    }

    //In this function the two entered email will be checked if the are a validate and will update when the entered password is matched
    public function update_email($email, $confirm_email, $password, $user_session_id, $user_session_id_for_insert) {
        $error = false;
        if($email === $confirm_email) {
            $error = Validation::check_email_validate($email);
        } else {
            $error = true;
        }
        if(!$error) {
            $error = $this->check_entered_password_with_user_password($password, $user_session_id);
            if(!$error) {
                $database = DatabaseFactory::getFactory()->getConnection();
                $sql_update_email = "UPDATE users SET email = ? WHERE id = ?";
                $statement = $database->prepare($sql_update_email);
                $statement->bind_param("si", $email, $user_session_id_for_insert);
                $statement->execute();
            } else {
                $error = true;
            }
        }
        return $error;
    }

    //In this function the old password will be checked and and new password will be hased
    public function update_password($old_password, $new_password, $confirm_password, $user_session_id, $user_session_id_for_insert) {
        $error = false;
        if($new_password === $confirm_password) {
            $error = Validation::check_password_with_confirmpassword($new_password, $confirm_password);
        } else {
            $error = true;
        }
        if(!$error) {
            $error = $this->check_entered_password_with_user_password($old_password, $user_session_id);
            if(!$error) {
                $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
                $database = DatabaseFactory::getFactory()->getConnection();
                $sql_update_password = "UPDATE users SET `password` = ? WHERE id = ?";
                $statement = $database->prepare($sql_update_password);
                $statement->bind_param("si", $password_hash, $user_session_id_for_insert);
                $statement->execute();
            } else {
                $error = true;
            }
        }
        return $error;
    }

    //In this function the user with the enterd session will be removed!
    public static function remove_user($user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_delete_user = "DELETE FROM users WHERE id = ?";
        $statement = $database->prepare($sql_delete_user);
        $statement->bind_param("i", $user_session_id);
        $statement->execute();
    }
}

?>