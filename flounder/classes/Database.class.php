<?php
/** 
 * Creating a class for the database connection
 * To start using the database connection like this: $database = DatabaseFactory::getFactory()->getConnection();
*/

class DatabaseFactory {
    protected $servername = "mysql";
    protected $username = "root";
    protected $password = "root";
    protected $dbname = "flounder";
    private static $factory;
    private $database;

    public static function getFactory() {
        if(!self::$factory) {
            self::$factory = new DatabaseFactory();
        }
        return self::$factory;
    }

    public function getConnection() {
        if(!$this->database) {
            try {
                $this->database = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
            } catch (mysqli_sql_exception $e) {
                $error = $e->getMessage();
                echo "Error:" .$error;
                exit;
            }
        }
        return $this->database;
    }
}