<?php
/**
 * This class useredit has the needed functions for the user to be edit
*/

class Useredit {
    
    //This function will update the user information by the admin
    public static function update_user_information_by_admin($firstname_update, $lastname_update, $date_of_birth_update, $phone_update, $country_update, $role_type_id_update, $user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_update_user_information_by_admin = "UPDATE users SET firstname = ?, lastname = ?, date_of_birth = ?, phone = ?, country = ?, role_type_id = ?  WHERE id = ?";
        $statement = $database->prepare($sql_update_user_information_by_admin);
        $statement->bind_param("sssssii", $firstname_update, $lastname_update, $date_of_birth_update, $phone_update, $country_update, $role_type_id_update, $user_session_id);
        $statement->execute();
    }

    //This function will select everything from users table
    public static function select_everything_from_users_table() {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_everything_from_users = "SELECT * FROM users";
        $statement = $database->prepare($sql_select_everything_from_users);
        $statement->execute();
        $result = $statement->get_result();
        return $result;
    }
}
?>