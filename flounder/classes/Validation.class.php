<?php
/**
 * This Class has all the check functions
*/

class Validation {

    private static $factory;

    //This part will create a new class for me
    public static function getFactory() {
        if(!self::$factory) {
            self::$factory = new Validation();
        }
        return self::$factory;
    }

    //With this funtion the email will be checked if it exists in the database
    public static function check_email_exists($email) {
        $error = false;
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM users WHERE email = ?";
        $statement = $database->prepare($sql);
        $statement->bind_param('s', $email);
        $statement->execute();
        $result = $statement->get_result();
        if($result->num_rows !== 0) {
            echo 'This Email address is already taken.<br>';
            $error = true;
        }
        return $error;
    }

    //This function checks if the entered email is valid
    public static function check_email_filter($email) {
        $error = false;
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo 'Please Enter a valid Email address.<br>';
            $error = true;
        }
        return $error;
    }

    //This function will include the check_email_filter function and the check_email_exists function!
    public static function check_email_validate($email) {
        $error = false;
        $error = self::check_email_filter($email);
        if(!$error) {
            $error = self::check_email_exists($email);
        }
        return $error;
    }

    //This function checks if a password was entered and matches the confirm password
    public static function check_password_with_confirmpassword($password, $confirmpassword) {
        $error = false;
        if(strlen($password) == 0 || strlen($confirmpassword) == 0) {
            echo 'Please Enter a Password.<br>';
            $error = true;
        }
        if($password !== $confirmpassword) {
            echo 'The Passwords must match.<br>';
            $error = true;
        }
        return $error;
    }

    public static function check_password_if_empty($password) {
        $error = false;
        if(strlen($password) == 0 || strlen($password) == "") {
            echo 'Please Enter a Password.<br>';
            $error = true;
            die();
        }
        if(!isset($password) || $password == "") {
            echo 'The Password is empty.<br>';
            $error = true;
            die();
        }
        return $error;
    }

    //This function will send view the checksessions page
    public static function view_checksessions_page() {
        echo("<script>location.href = '?p=checksessions';</script>");
        exit;
    }

    //This function will send view the errorcourseview page
    public static function view_errorcourseview_page() {
        echo("<script>location.href = '?p=errorcourseview';</script>");
        exit;
    }

    //This function will send view the course page
    public static function view_course_page() {
        echo("<script>location.href = '?p=course';</script>");
        exit;
    }

    //This function will send view the users page
    public static function view_users_page() {
        echo("<script>location.href = '?p=users';</script>");
        exit;
    }

    //This function will send view the login page
    public static function view_login_page() {
        echo("<script>location.href = '?p=login';</script>");
        exit;
    }

    //This function will send view the profile page
    public static function view_profile_page() {
        echo("<script>location.href = '?p=profile';</script>");
        exit;
    }

    //This function will send view the home page
    public static function view_home_page() {
        echo("<script>location.href = '?p=home';</script>");
        exit;
    }

    //This function will send view the registerforcourses page
    public static function view_registerforcourses_page() {
        echo("<script>location.href = '?p=registerforcourses';</script>");
        exit;
    }

    //This function will send view the verifyusersintocourses page
    public static function view_verifyusersintocourses_page() {
        echo("<script>location.href = '?p=verifyusersintocourses';</script>");
        exit;
    }
}
?>