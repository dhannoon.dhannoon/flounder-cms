<?php
/**
 * This class has the functions to create new users
*/

class Createusers {

    //This function will create/register a new user
    public static function create_users($firstname, $lastname, $email, $password, $date_of_birth, $role_type_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $passwort_hash = password_hash($password, PASSWORD_DEFAULT);
        $sql_create_user = "INSERT INTO users (firstname, lastname, email,`password`, date_of_birth, role_type_id ) VALUES (?, ?, ?, ?, ?, ?)";
        $statement = $database->prepare($sql_create_user);
        $statement->bind_param('sssssi', $firstname, $lastname, $email, $passwort_hash, $date_of_birth, $role_type_id);
        $result = $statement->execute();
        return $result;
    }
}
?>