<?php
/** 
 * This Class has all the functions what is needed to create course and manage the course
 * Most of the function are public and static
*/

class Createcourse {

    //This function will select for me everything from table course by the userid
    public static function select_everything_from_course_by_user_id($user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_course_table_by_user_id = "SELECT * FROM course WHERE userid = ?";
        $statement = $database->prepare($sql_select_course_table_by_user_id);
        $statement->bind_param("i", $user_session_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        $course_by_userid = $result->fetch_assoc();
        return $course_by_userid;
    }

    //This function will select for me everything from table course
    public static function select_everything_from_course() {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_course_table = "SELECT * FROM course";
        $statement = $database->prepare($sql_select_course_table);
        $result = $statement->execute();
        $result = $statement->get_result();
        return $result;
    }

    //This function will select everything form course table by the course id
    public static function select_everything_from_course_by_course_id($course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM course WHERE id = ?";
        $statement = $database->prepare($sql);
        $statement->bind_param("i", $course_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        $course_id = $result->fetch_assoc();
        return $course_id;
    }

    //This function will select everything form course table by the course id
    public static function select_everything_from_course_by_course_id_without_fetch($course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM course WHERE id = ?";
        $statement = $database->prepare($sql);
        $statement->bind_param("i", $course_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        return $result;
    }

    //This function will create a new course
    public static function create_course($course_name, $course_start, $course_end, $exam_date, $point, $user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_create_course = "INSERT INTO course (course_name, course_start, course_end, exam_date, point, userid) VALUES (?, ?, ?, ?, ?, ?)";
        $statement = $database->prepare($sql_create_course);
        $statement->bind_param('sssssi', $course_name, $course_start, $course_end, $exam_date, $point, $user_session_id);
        $result = $statement->execute();
        return $result;
    }

    //This function will select everything from user where the user in course is!
    public static function select_courses_where_user_id($course_id, $user_session_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_join_user_id_course_id = "SELECT `users`.*, course.* FROM `users`
        LEFT JOIN course ON course.id = ?
        JOIN `course_participants` ON `users`.`id` = `course_participants`.`user_id` 
        WHERE `course_participants`.`course_id` = ? AND `users`.`id` = ?";
        $statement = $database->prepare($sql_join_user_id_course_id);
        $statement->bind_param("iii", $course_id, $course_id, $user_session_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        $user = $result->fetch_assoc();
        return $user;
    }

    //This function will select all the users from the database by the role_type_id
    public static function select_all_users_by_role_type_id($role_type_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_all_users_by_role_type_id = "SELECT * FROM users WHERE role_type_id = ?";
        $statement = $database->prepare($sql_select_all_users_by_role_type_id);
        $statement->bind_param("i", $role_type_id);
        $statement->execute();
        $users = $statement->get_result();
        return $users;
    }

    //This function will delete all the users from the course
    public static function delete_users_from_course($course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_delete_users_from_course = "DELETE FROM course_participants WHERE course_id = ?";
        $statement = $database->prepare($sql_delete_users_from_course);
        $statement->bind_param("i", $course_id);
        $result = $statement->execute();
        return $result;
    }

    //This function will delete all the users from the course
    public static function delete_users_from_course_where_course_id_and_user_id($course_id, $user_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_delete_users_from_course_where_course_id_and_user_id = "DELETE FROM course_participants WHERE course_id = ? AND user_id = ?";
        $statement = $database->prepare($sql_delete_users_from_course_where_course_id_and_user_id);
        $statement->bind_param("ii", $course_id, $user_id);
        $result = $statement->execute();
        return $result;
    }

    //This function will update the course information
    public static function update_course_id($course_name, $course_start, $course_end, $exam_date, $point, $course_html, $course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_update_course = "UPDATE course SET course_name = ?, course_start = ?, course_end = ?, exam_date = ?, point = ?, course_html = ? WHERE id = ?";
        $statement = $database->prepare($sql_update_course);
        $statement->bind_param("ssssssi", $course_name, $course_start, $course_end, $exam_date, $point, $course_html, $course_id);
        $result = $statement->execute();
        return $result;
    }

    //This function will select for me the user_id from table course_participants where the course_id is set
    public static function select_user_id_from_course_participants_by_course_id($course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_user_id_by_course_id = "SELECT user_id FROM course_participants WHERE course_id = ?";
        $statement = $database->prepare($sql_select_user_id_by_course_id);
        $statement->bind_param("i", $course_id);
        $statement->execute();
        $result = $statement->get_result();
        return $result;
    }

    //This function will select for me the user_id from table course_participants where the course_id is set
    public static function select_all_from_course_participants_by_user_id($user_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_all_by_user_id = "SELECT * FROM course_participants WHERE user_id = ?";
        $statement = $database->prepare($sql_select_all_by_user_id);
        $statement->bind_param("i", $user_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        return $result;
    }

    //This function will add user into course
    public static function add_user_into_course($user_id, $course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_add_user_into_course = "INSERT INTO course_participants (user_id, course_id) VALUES (?, ?)";
        $statement = $database->prepare($sql_add_user_into_course);
        $statement->bind_param("ii", $user_id, $course_id);
        $statement->execute();
    }

    //This function will delete the course by the course_id
    public static function remove_course_by_admin($course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_delete_course = "DELETE FROM course WHERE id = ?";
        $statement = $database->prepare($sql_delete_course);
        $statement->bind_param("i", $course_id);
        $result = $statement->execute();
        return $result;
    }

    //This function will insert into the verify_users_into_courses (user_id, course_id, verify)
    public static function add_user_into_verify_course_table($user_id, $course_id, $verify) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_insert_into_the_verify_users_into_courses_table = "INSERT INTO verify_users_into_courses (user_id, course_id, verify) VALUES (?, ?, ?)";
        $statement = $database->prepare($sql_insert_into_the_verify_users_into_courses_table);
        $statement->bind_param("iii", $user_id, $course_id, $verify);
        $result = $statement->execute();
        return $result;
    }

    //This function will update the verify value where user_id and course_id are given!
    public static function update_user_into_verify_course_table_where_userid_and_course_id($verify, $user_id, $course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_update_the_verify_users_into_courses_table = "UPDATE verify_users_into_courses SET verify = ? WHERE user_id = ? AND course_id = ?";
        $statement = $database->prepare($sql_update_the_verify_users_into_courses_table);
        $statement->bind_param("iii", $verify, $user_id, $course_id);
        $result = $statement->execute();
        return $result;
    }

    //This function will select everything from the table verify_users_into_courses where verify is entered 
    public static function select_everything_from_verify_users_into_courses($verify) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_everything_from_verify_users_into_courses = "SELECT * FROM verify_users_into_courses WHERE verify = ?";
        $statement = $database->prepare($sql_select_everything_from_verify_users_into_courses);
        $statement->bind_param("i", $verify);
        $result = $statement->execute();
        $result = $statement->get_result();
        return $result;
    }

    //This function will select everything from the table verify_users_into_courses where the user_id and the course_id are entered
    public static function select_verify_where_course_id_and_user_id($user_id, $course_id) {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql_select_everything_from_verify_users_into_courses = "SELECT * FROM verify_users_into_courses WHERE user_id = ? AND course_id = ?";
        $statement = $database->prepare($sql_select_everything_from_verify_users_into_courses);
        $statement->bind_param("ii", $user_id, $course_id);
        $result = $statement->execute();
        $result = $statement->get_result();
        $is_verify = $result->fetch_assoc();
        return $is_verify;
    }
}
?>