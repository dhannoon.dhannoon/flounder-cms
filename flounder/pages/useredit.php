<?php
if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
    require(__DIR__.'/logic/useredit.logic.php');
?>
<div class="w3-container">
    <div id="useredit" class="useredit">
        <form class="w3-modal-content animate" action="" method="post" enctype="multipart/form-data">
            <div class="imagecontainer">
                <?php
                    if($user["profile_image"] !== null) {
                        $imageURL = "/flounder/pages/image/image_users/".$user["profile_image"];
                ?>
                        <img src="<?php echo $imageURL; ?>" alt="logo" class="logo" width="350" height="250"/>
                <?php
                    } else if($user["profile_image"] === null) {
                            $user_logo = "/flounder/pages/image/user_logo.png";
                    ?>    <img src="<?php echo $user_logo; ?>" alt="logo" class="logo" width="250" height="150"/>
                    <?php
                    }
                ?>
            </div>
            <div class="w3-container">
                <label for="user_information">
                    <h2>User Information</h2>
                </label><br>
                <label for="firstname">First Name</label>
                    <input type="text" class="w3-input-field" value="<?php echo $user["firstname"]; ?>" id="firstname_update_by_admin" name="firstname_update_by_admin">
                <label for="lastname">Last Name</label>
                    <input type="text" class="w3-input-field" value="<?php echo $user["lastname"]; ?>" id="lastname_update_by_admin" name="lastname_update_by_admin">
                <label for="date_of_birth">Date of Birth</label>
                    <input type="date" class="w3-input-field" value="<?php echo $user["date_of_birth"]; ?>" id="date_of_birth_update_by_admin" name="date_of_birth_update_by_admin">
                <label for="phone">Phone</label>
                    <input type="tel" class="w3-input-field" value="<?php echo $user["phone"]; ?>" id="phone_update_by_admin" name="phone_update_by_admin">
                <label for="country">Country</label>
                    <select class="w3-input-field" id="country" name="country_update_by_admin">
                        <option id="Austria" value="Austria"<?php if ($user["country"] == 'Austria') echo ' selected="selected"'; ?>>Austria</option>
                        <option id="Germany" value="Germany"<?php if ($user["country"] == 'Germany') echo ' selected="selected"'; ?>>Germany</option>
                        <option id="Another Country" value="Another Country"<?php if ($user["country"] == 'Another Country') echo ' selected="selected"'; ?>>Another Country</option>
                    </select>
                <label for="role_type">Role Type</label>
                    <select class="w3-input-field" id="role_type" name="role_type_id_by_admin" >
                        <option id="Admin" value="1"<?php if ($user["role_type_id"] == '1') echo ' selected="selected"'; ?>>Admin</option>
                        <option id="Teacher" value="2"<?php if ($user["role_type_id"] == '2') echo ' selected="selected"'; ?>>Teacher</option>
                        <option id="Student" value="3"<?php if ($user["role_type_id"] == '3') echo ' selected="selected"'; ?>>Student</option>
                    </select>
                <label for="upload_image">Select image to upload</label>
                    <input type="file" class="w3-input-field" class="button" id="uploadimage_by_admin" name="uploadimage_update_by_admin">                    
                <button type="submit" class="button" id="change_user_information">Update your information</button>
            </div>
        </form>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="w3-container">
                <label for="email_account">
                    <h2>E-Mail Account</h2>
                </label><br>
                <label for="current_email"><b>User Current E-Mail</b></label>
                    <input type="email" class="w3-input-field" value="<?php echo $user["email"]; ?>" id="user_current_email" readonly>
                <label for="user_email_update"><b>User New E-Mail</b></label>
                    <input type="email" class="w3-input-field" placeholder="Update user E-Mail here" id="user_email_update" name="user_email_update" require>
                <label for="confirm_user_email_update"><b>Confirm The New User E-Mail</b></label>
                    <input type="email" class="w3-input-field" placeholder="Confirm User E-Mail here" id="confirm_user_email_update" name="confirm_user_email_update" require>
                <label for="admin_password_update_user_email"><b>Admin Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter The Admin Password here" id="admin_password_update_user_email" name="admin_password_update_user_email" require>
                <button type="submit" class="button" id="change_email_account_by_admin">Change Email</button>
            </div>
        </form>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="w3-container">
                <label for="change_password_by_admin">
                    <h2>Change User Password</h2>
                </label><br>
                <label for="admin_password"><b>Admin Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter the Admin password here" id="admin_password" name="admin_password" require>
                <label for="user_new_password_update"><b>User New Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter the new user password here" id="user_new_password_update" name="user_new_password_update" require>
                <label for="confirm_user_new_password_update"><b>Confirm The New User Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Confirm the new user password here" id="confirm_user_new_password_update" name="confirm_user_new_password_update" require>
                <button type="submit" class="button" id="change_user_password_by_admin">Change Password</button>
            </div>
        </form>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="w3-container">
                <label for="remove_account"><h2>Remove Account</h2></label>
                    <button type="submit" class="button" id="remove_account" name="remove_account_by_admin">Remove Account</button>
            </div>
        </form>
        <div class="w3-container footer">
            <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
        </div>
    </div>
</div>
<?php
} else {
    Session::destroy();
    echo '<script> alert("User can not visit this page");</script>';
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    // Get the modal
    var modal = document.getElementById('login');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>