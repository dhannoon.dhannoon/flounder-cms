<?php
require(__DIR__.'/logic/courseview.logic.php');
?>
<div class="w3-container">
    <div id="courseview" >
        <div class="w3-modal-content animate">
            <div class="w3-container">
                <div class="centertext" >
                    <h2>
                        <?php
                            view_welcoming_message($user, $course);
                        ?>
                    </h2>
                </div>
                <div>
                    <?php
                        echo $course["course_html"];
                    ?>
                </div>
                <!--<details>
                    <summary>session one</summary>
                        <p>
                            <?php
                                // echo $course["course_html"];
                            ?>
                        </p>
                </details>-->
                <!--<details>
                    <summary>session two</summary>
                    <p>here is the first session of the course.</p>
                </details>-->
                <div class="w3-container footer">
                    <button type="button" onclick="window.location.href = '?p=course';" class="cancelbtn" id="back_to_courses">Back to Courses</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>