<?php
require(__DIR__.'/logic/registerforcourses.logic.php');
$database = DatabaseFactory::getFactory()->getConnection();
?>
<div class="w3-container">
    <div id="registerforcourses">
        <div class="w3-modal-content animate">
            <div class="w3-container">
                <div class="centertext">
                    <h2>Register For Courses</h2>
                </div>
                <?php
                    if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
                        view_all_courses_for_student_to_register($all_information_from_course_table);
                    } else {
                        Session::destroy();
                        Validation::view_checksessions_page();
                        die();
                    }
                ?>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>