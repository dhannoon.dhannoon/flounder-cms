<?php
$database = DatabaseFactory::getFactory()->getConnection();
require(__DIR__.'/logic/users.logic.php');
?>
<div class="w3-container">
    <div id="users">
        <div class="w3-modal-content animate">
            <div  class="w3-container">
                <div class="centertext">
                    <h2>Users</h2>
                </div>
                <?php
                    if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
                        view_all_users_for_admin($users);
                    } else {
                        Session::destroy();
                        Validation::view_checksessions_page();
                        die();
                    }
                ?>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>