</body>
<!-- Include external JS libs. -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@latest/js/froala_editor.pkgd.min.js"></script>
<script src="/flounder/js/jquery.multi-select.js" type="text/javascript"></script>
<script>
var editor = new FroalaEditor('#froala_editor', {
    // toolbarButtons: ['bold', 'italic', 'html']
});
</script>
</html>