<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $location = "You are on ";
                 $pages_name = array("home", "course", "createcourse", "courseedit", "courseview", "users", "createusers", "useredit", "profile", "logout", "login", "register", "contact", "about", "errorcourseview", "resetpassword", "404", "checksessions", "registerforcourses", "verifyusersintocourses");
                 $get_page = $_GET["p"];
                 if(in_array($get_page, $pages_name)) {
                     echo $location . $get_page." page";
                 }
            ?>
    </title>
    <!--include the responsive navbar-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="/flounder/css/main.css" media="screen" rel="stylesheet" type="text/css">
    <!--include the responsive navbar-->
    <!-- Include Editor style. -->
        <link href="https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css" media="screen" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href="/flounder/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
    <!-- Include Editor style. -->
    <!--This is for the user table from datatables.net-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css">-->
        <link href="/flounder/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" media="screen" rel="stylesheet" type="text/css">
    <!--This is for the user table from datatables.net-->
</head>
<body id="w3-body">
<div class="w3-topnav" id="myTopnav">
    <a href="?p=home" class="a-after-topnav <?php if($_GET["p"] === "home") {  echo "w3-active"; } ?>" id="home">Home</a>
    <?php
        if(isset($_SESSION["user"]["id"])) {
    ?>
            <div class="w3-dropdown <?php if($_GET["p"] === "course") {  echo "w3-active"; } else if($_GET["p"] === "createcourse") {  echo "w3-active"; } else if($_GET["p"] === "verifyusersintocourses") {  echo "w3-active"; } else if($_GET["p"] === "registerforcourses") {  echo "w3-active"; } else if($_GET["p"] === "courseview") {  echo "w3-active"; } else if($_GET["p"] === "courseedit") {  echo "w3-active"; } ?>">
                <button class="w3-dropbtn" id="course">Course 
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="w3-dropdown-content">
                    <?php
                        if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
                            echo "<a class='a-after-dropdown-content' id='courses' href='?p=course'>Course</a>";
                            echo "<a class='a-after-dropdown-content' id='create_course' href='?p=createcourse'>Create Courses</a>";
                            echo "<a class='a-after-dropdown-content' id='verify_users_into_courses' href='?p=verifyusersintocourses'>Verify Users Into Courses</a>";
                        } else if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 2) {
                            echo "<a class='a-after-dropdown-content' id='courses' href='?p=course'>Course</a>";
                            echo "<a class='a-after-dropdown-content' id='create_course' href='?p=createcourse'>Create Courses</a>";
                        } else {
                            echo "<a class='a-after-dropdown-content' id='courses' href='?p=course'>Course</a>";
                            echo "<a class='a-after-dropdown-content' id='register_for_courses' href='?p=registerforcourses'>Register For Courses</a>";
                        }
                    ?>
                </div>
            </div>
            <?php
                if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
            ?>
                    <div class="w3-dropdown <?php if($_GET["p"] === "users") {  echo "w3-active"; } else if($_GET["p"] === "createusers") {  echo "w3-active"; } else if($_GET["p"] === "useredit") {  echo "w3-active"; } ?>">
                        <button class="w3-dropbtn" id="users">Users 
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="w3-dropdown-content">
                            <?php
                                echo "<a class='a-after-dropdown-content' id='users' href='?p=users'>users</a>";
                                echo "<a class='a-after-dropdown-content' id='create_users' href='?p=createusers'>Create users</a>";
                            ?>
                        </div>
                    </div>
            <?php
                }
            ?>
    <?php
        }
    ?>
    <div class="w3-dropdown <?php 
                                if($_GET["p"] === "profile") {
                                    echo "w3-active"; 
                                } else if($_GET["p"] === "logout") { 
                                    echo "w3-active"; 
                                } else if($_GET["p"] === "login") {
                                    echo "w3-active";
                                } else if($_GET["p"] === "register") {
                                    echo "w3-active";
                                }
                        ?>">
        <button class="w3-dropbtn" id="profile"><?php if(!empty($_SESSION["user"])) {echo "Profile";} else { echo "My Flounder";} ?> 
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="w3-dropdown-content">
            <?php
                if(isset($_SESSION["user"]["id"])) {
                    echo "<a class='a-after-dropdown-content' id='my_account' href='?p=profile'>My Account</a>";
                    echo "<a class='a-after-dropdown-content' id='logout' href='?p=logout'> Log Out</a>";
                }
                else {
                    echo "<a class='a-after-dropdown-content' id='login' href='?p=login'>Login</a>";
                    echo "<a class='a-after-dropdown-content' id='register' href='?p=register'>Register</a>";
                }
            ?>
        </div>
    </div>
    <a href="?p=contact" class="a-after-topnav <?php if($_GET["p"] === "contact") {  echo "w3-active"; } ?>" id="contact">Contact</a>
    <a href="?p=about" class="a-after-topnav <?php if($_GET["p"] === "about") {  echo "w3-active"; } ?>" id="about">About</a>
    <a href="javascript:void(0);" style="font-size:15px;" class="a-after-topnav icon" onclick="myFunction()">&#9776;</a>
</div>