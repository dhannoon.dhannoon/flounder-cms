<?php
if(isset($_SESSION["user"])){
    require(__DIR__.'/logic/createusers.logic.php');
?>
<div class="w3-container">
    <div id="createuser">
        <?php
        # call the showFormular if it is false then do not show the form!
        if($showFormular) {
        ?>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="w3-container">
                <div class="centertext">
                    <h2>Create users</h2>
                </div>
                <label for="firstname"><b>First Name</b></label>
                    <input type="text" class="w3-input-field" maxlength="100" placeholder="Enter your First Name here" id="firstname" name="firstname_create" require>
                <label for="lastname"><b>Last Name</b></label>
                    <input type="text" class="w3-input-field" maxlength="100" placeholder="Enter your Last Name here" id="lastname" name="lastname_create" require>
                <label for="email"><b>E-Mail</b></label>
                    <input type="email" class="w3-input-field" maxlength="100" placeholder="Enter your E-mail Address" id="email" name="email_create" require>
                <label for="date_of_birth"><b>Date of Birth</b></label>
                    <input type="date" class="w3-input-field" id="date_of_birth" name="date_of_birth_create" require>
                <label for="password"><b>Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your Password here!" id="password" name="password_create" require>
                <label for="confirmpassword"><b>Confirm Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your Password Again" id="confirmpassword" name="confirmpassword_create">
                <label for="role_type">Role Type</label>
                <select class="w3-input-field" id="role_type" name="role_type_create" >
                    <option value="1">Admin</option>
                    <option value="2">Teacher</option>
                    <option value="3">Student</option>
                </select>
                <button type="submit" class="button" id="submit_create_user">Register</button>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
            </div>
        </form>
        <?php
        } //Ende from the if($showFormular)
        ?>
    </div>
</div>
<?php
} else {
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    // Get the modal
    var modal = document.getElementById('register');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>