<?php
if(isset($_SESSION["user"])){
    require(__DIR__.'/logic/profile.logic.php');
?>
<div class="w3-container">
    <div id="profile">
        <form class="w3-modal-content animate" action="" method="post" enctype="multipart/form-data">
            <div class="imagecontainer">
                <?php
                    if($user["profile_image"] !== null) {
                        $imageURL = "/flounder/pages/image/image_users/".$user["profile_image"];
                ?>
                        <img src="<?php echo $imageURL; ?>" alt="logo" class="logo" width="350" height="250"/>
                <?php
                    } else if($user["profile_image"] === null) {
                            $user_logo = "/flounder/pages/image/user_logo.png";
                    ?>    <img src="<?php echo $user_logo; ?>" alt="logo" class="logo" width="250" height="150"/>
                    <?php
                    }
                ?>
            </div><br><br>
            <div class="centertext">
                <h2>User Information</h2>
            </div>
            <div class="w3-container">
                <label for="firstname">First Name</label>
                    <input type="text" class="w3-input-field" value="<?php echo $user["firstname"]; ?>" id="firstname" name="firstname_update">
                <label for="lastname">Last Name</label>
                    <input type="text" class="w3-input-field" value="<?php echo $user["lastname"]; ?>" id="lastname" name="lastname_update">
                <label for="date_of_birth">Date of Birth</label>
                    <input type="date" class="w3-input-field" value="<?php echo $user["date_of_birth"]; ?>" id="date_of_birth" name="date_of_birth_update">
                <label for="phone">Phone</label>
                    <input type="tel" class="w3-input-field" value="<?php echo $user["phone"]; ?>" id="phone" name="phone_update">
                <label for="country">Country</label>
                    <select class="w3-input-field" id="country" name="country_update">
                        <option id="Austria" value="Austria"<?php if ($user["country"] == 'Austria') echo ' selected="selected"'; ?>>Austria</option>
                        <option id="Germany" value="Germany"<?php if ($user["country"] == 'Germany') echo ' selected="selected"'; ?>>Germany</option>
                        <option id="Another Country" value="Another Country"<?php if ($user["country"] == 'Another Country') echo ' selected="selected"'; ?>>Another Country</option>
                    </select>
                <label for="role_type_name">Role</label>
                    <input type="text" class="w3-input-field" id="role_type_name" name="role_type_name" value="<?php if ($user["role_type_id"] === 1){echo "Admin";} else if($user["role_type_id"] === 2) { echo "Teacher";} else if($user["role_type_id"] === 3) { echo "Student";} else {Validation::view_checksessions_page(); die();}  ?>" readonly>
                <label for="upload_image">Select image to upload</label>
                    <input type="file" class="w3-input-field" class="button" id="uploadimage" name="uploadimage_update">
                <button type="submit" class="button" id="change_user_information">Update your information</button>
            </div>
        </form>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="centertext">
                <h2>E-Mail Account</h2>
            </div>
            <div class="w3-container">
                <label for="current_email"><b>Your Current E-Mail</b></label>
                    <input type="email" class="w3-input-field" value="<?php echo $user["email"]; ?>" id="current_email" readonly>
                <label for="email_update"><b>New E-Mail</b></label>
                    <input type="email" class="w3-input-field" placeholder="Update your E-Mail" id="email_update" name="email_update" require>
                <label for="confirmemail_update"><b>Confirm E-Mail</b></label>
                    <input type="email" class="w3-input-field" placeholder="Enter your E-Mail again here!" id="confirmemail_update" name="confirmemail_update" require>
                <label for="password"><b>Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your Password here!" id="password_update_email" name="password_update_email">
                <button type="submit" class="button" id="change_email_account">Change Email</button>
            </div>
        </form>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="centertext">
                <h2>Change Password</h2>
            </div>
            <div class="w3-container">
                <label for="old_password"><b>Old Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your old password" id="old_password" name="password">
                <label for="new_password_update"><b>New Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Update your password" id="new_password_update" name="password_update" require>
                <label for="new_confirmpassword_update"><b>Confirm Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Confirm your updated password" id="new_confirmpassword_update" name="confirmpassword_update" require>
                <button type="submit" class="button" id="change_your_password">Change Password</button>
            </div>
        </form>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="centertext">
                <h2>Remove Account</h2>
            </div>
            <div class="w3-container">
                <button type="submit" class="button" id="remove_account" name="remove_account">Remove Account</button>
            </div>
        </form>
        <div class="w3-container footer">
            <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
            <!--<span class="remove_account"><a href="#">Remove Account</a></span>-->
            <!--<span class="password">Register <a href="?p=register">Register?</a></span>-->
        </div>
    </div>
</div>
<?php
} else {
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    // Get the modal
    var modal = document.getElementById('login');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>