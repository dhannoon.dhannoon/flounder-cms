<?php
require(__DIR__.'/logic/course.logic.php');
$database = DatabaseFactory::getFactory()->getConnection();
?>
<div class="w3-container">
    <div id="course">
        <div class="w3-modal-content animate">
            <div class="w3-container">
                <div class="centertext">
                    <h2>Courses</h2>
                </div>
                <?php
                    if(!empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {
                        view_all_courses_for_admin($all_from_course_table);
                    } else if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
                        student_in_course($student_are_assigned_into_course_id);
                    } else {
                        Session::destroy();
                        Validation::view_checksessions_page();
                        die();
                    }
                ?>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>