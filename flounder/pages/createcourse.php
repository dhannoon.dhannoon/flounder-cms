<?php
if(isset($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {
    require(__DIR__.'/logic/createcourse.logic.php');
?>
<div class="w3-container">
    <div id="create_course">
        <?php
        if($showFormular) {
        ?>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="w3-container">
                <div class="centertext">
                    <h2>Create course</h2>
                </div>
                <label for="course_name">Course Name</label>
                    <input type="text" class="w3-input-field" placeholder="Enter The Course Name" id="course_name" name="course_name">
                <label for="course_start">Course Start</label>
                    <input type="date" class="w3-input-field" id="course_start" name="course_start">
                <label for="course_end">Course End</label>
                    <input type="date" class="w3-input-field" id="course_end" name="course_end">
                <label for="exam_date">Exam Date</label>
                    <input type="date" class="w3-input-field" id="exam_date" name="exam_date">
                <label for="course_point">Course Point</label>
                    <input type="text" class="w3-input-field" placeholder="Enter your Course Point" id="course_point" name="course_point">
                <button type="submit" class="button" id="submit_to_create_course">Create course</button>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn" id="cancel">Cancel</button>
            </div>
        </form>
        <?php
        }
        ?>
    </div>
</div>
<?php
} else {
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>