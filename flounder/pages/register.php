<?php
require(__DIR__.'/logic/register.logic.php');
?>
<div class="w3-container">
    <!--<button onclick="document.getElementById('register').style.display='block'" style="width:auto;">Register</button>-->
    <div id="register">
        <?php
        # call the showFormular if it is false then do not show the form!
        if($showFormular) {
        ?>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="centertext">
                <h2>Register</h2>
            </div>
            <div class="w3-container">
                <label for="firstname"><b>First Name</b></label>
                    <input type="text" class="w3-input-field" maxlength="100" placeholder="Enter your First Name here!" id="firstname" name="firstname" require>
                <label for="lastname"><b>Last Name</b></label>
                    <input type="text" class="w3-input-field" maxlength="100" placeholder="Enter your Last Name here!" id="lastname" name="lastname" require>
                <label for="email"><b>E-Mail</b></label>
                    <input type="email" class="w3-input-field" maxlength="100" placeholder="Enter your E-mail Address" id="email" name="email" require>
                <label for="date_of_birth"><b>Date of Birth</b></label>
                    <input type="date" class="w3-input-field" id="date_of_birth" name="date_of_birth" require>
                <label for="password"><b>Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your Password here!" id="password" name="password" require>
                <label for="confirmpassword"><b>Confirm Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your Password Again" id="confirmpassword" name="confirmpassword">
                <button type="submit" class="button" id="submit_register" >Register</button>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn" id="cancel">Cancel</button>
                <span class="password">Forgot <a href="?p=resetpassword" id="resetpassword">password?</a></span>
            </div>
            <div id="sign_in" class="w3-container signin">
                <p>Already have an account? <a href="?p=login" id="login">Sign in</a>.</p>
            </div>
        </form>
        <?php
        } //Ende from the if($showFormular)
        ?>
    </div>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('register');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>