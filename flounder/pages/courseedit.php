<?php
require(__DIR__.'/logic/courseedit.logic.php');
?>
<div class="w3-container">
    <div id="courseedit">
        <form class="w3-modal-content animate" action="" method="post">
            <div class="w3-container">
                <div for="course_information">
                    <h1 class="centertext">Course Information</h1>
                </div>
                <div>
                    <label for="course_name">Course Name</label>
                        <input type="text" class="w3-input-field" value="<?php echo $course["course_name"]; ?>" id="course_name" name="course_name">
                </div>
                <div>
                    <label for="course_create_at">Course Created at</label>
                        <input type="text" class="w3-input-field" value="<?php echo $course["course_create_at"]; ?>" id="course_create_at" readonly>
                </div>
                <div>
                    <label for="course_start">Course Start</label>
                        <input type="date" class="w3-input-field" value="<?php echo $course["course_start"]; ?>" id="course_start" name="course_start">
                </div>
                <div>
                    <label for="course_end">Course End</label>
                        <input type="date" class="w3-input-field" value="<?php echo $course["course_end"]; ?>" id="course_end" name="course_end">
                </div>
                <div>
                    <label for="course_start">Exam Data</label>
                        <input type="date" class="w3-input-field" value="<?php echo $course["exam_date"]; ?>" id="exam_date" name="exam_date">
                </div>
                <div>
                    <label for="course_point">Course Point</label>
                        <input type="text" class="w3-input-field" value="<?php echo $course["point"]; ?>" id="point" name="point">
                </div>
                <br>
                <br>
                <div for="course_content">
                    <h1 class="centertext">Course Content</h1>
                </div>
                <div>
                    <div id="froala_editor">
                        <?php echo $course["course_html"]; ?>
                    </div>
                </div>
                <br>
                <br>
                <div for="user_relationship_to_course">
                    <h1 class="centertext">User Relationship To Course</h1>
                </div>
                <div>
                    <select multiple="multiple" id="selected_user" name="selected_users[]">
                        <?php 
                            while($user = $users->fetch_assoc()) {
                                $selected = in_array($user["id"] , $participants);
                                echo "<option " .($selected ? "selected='selected'" : "") . " value=".$user["id"]."> ".$user["email"]."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div>
                    <button type="button" onclick="location.href='<?php echo $navigate_to_current_course.'&delete_users=1';?>';" id="delete_all_the_users_from_course">Delete All The Users</button>
                </div>
                <br>
                <div>
                    <button type="button" class="button" onclick="location.href='pages/pdf/<?php echo $pdfName;?>';">Create PDF from the User List</button>
                </div>
                <br>
                <div>
                    <button type="submit" class="button" id="submit_the_form">Update The Course Information</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $( document ).ready(function() {
       var selectBox = $('#selected_user').multiSelect();
       console.log(selectBox);
        $( "#submit_the_form" ).click(function(e) {
            e.preventDefault();
            console.log(editor.html.get());
            alert( "Users has been submitted!" );
            //alert( $("#selected_user").val() );
            $.post( window.location.href, {
                course_html: editor.html.get(),
                course_name: $("#course_name").val(),
                course_start: $("#course_start").val(),
                course_end: $("#course_end").val(),
                exam_date: $("#exam_date").val(),
                point: $("#point").val(),
                add_user_into_course: $("#selected_user").val()
            });
        });
    });
</script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>