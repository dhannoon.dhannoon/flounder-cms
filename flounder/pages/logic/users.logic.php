<?php

$database = DatabaseFactory::getFactory()->getConnection();

if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {



    if(isset($_POST["deleteuser_where_user_id"]) && !empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {

        $user_id_to_delete = $_POST["deleteuser_where_user_id"];
    
        //This will remove the user by the admin
        if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
            $remove_user = Profile::remove_user($user_id_to_delete);
            if($remove_user !== null) {
                echo '<script> alert("Unfortunately, an error occurred while removing the user!");</script>';
                Validation::view_users_page();
                die();
            }
            if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
                echo '<script> alert("Removing the user did work!");</script>';
                Validation::view_users_page();
                die();
            } else {
                Session::destroy();
                Validation::view_checksessions_page();
                die();
            }
        } else {
            Session::destroy();
            Validation::view_checksessions_page();
            die();
        }
    }

    $users = Useredit::select_everything_from_users_table();

    function view_all_users_for_admin($users) {

        //Check if the returned result (num_rows) are zero
        if($users->num_rows == 0) {
            echo '<script> alert("Something happened while getting the users!");</script>';
            Session::destroy();
            Validation::view_checksessions_page();
            die();
        }

        //If the returned (all_from_course_table) true then -> fetch
        if($users) {
            echo " <table id='users_table' class='table table-striped table-bordered' style='width:100%;'>
                        <thead>
                            <tr>
                                <th align='left'>Number</th>
                                <th align='left'>Firstname</th>
                                <th align='left'>Lastname</th>
                                <th align='left'>E-Mail</th>
                                <th align='left'>Role</th>
                                <th align='left'>Edit</th>
                                <th align='left'>Delete</th>
                            </tr>
                        </thead>
                    <tbody>
            ";
            //Getting the course information in array!
            $users_counter = 0;
            while($user = $users->fetch_assoc()) {

                if($user) {
                    if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2) {
                        echo " 
                            <tr style='padding: 8px;'>
                                <td>";
                                $users_counter++;
                                echo $users_counter;
                        echo    "</td>
                                <td>".$user["firstname"]."</td>
                                <td>".$user["lastname"]."</td>
                                <td>".$user["email"]."</td>
                                <td>"; 
                                if ($user["role_type_id"] === 1){echo 'Admin';} else if($user["role_type_id"] === 2) { echo "Teacher";} else if($user["role_type_id"] === 3) { echo "Student";} else {Validation::view_checksessions_page();};
                       echo     "</td>
                                <td><a href='index.php?p=useredit&user_id=".$user["id"]."'>Edit</a>  </td>
                                <td>";
                                    ?>
                                        <form action="" method="post">
                                            <input type="hidden" name="deleteuser_where_user_id" value="<?php echo $user["id"]; ?>">
                                            <button type="submit">Delete</button>
                                        </form>
                                    <?php
                        echo    "</td>
                            </tr>
                       ";
                    
                    } else {
                        echo '<script> alert("Unfortunately, an error occurred while getting the course information from the database");</script>';
                        Session::destroy();
                        Validation::view_checksessions_page();
                        die();
                    }
                } 
            }
            echo "  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th align='left'>Number</th>
                            <th align='left'>Firstname</th>
                            <th align='left'>Lastname</th>
                            <th align='left'>E-Mail</th>
                            <th align='left'>Role</th>
                            <th align='left'>Edit</th>
                            <th align='left'>Delete</th>
                        </tr>
                    </tfoot>
                </table>"
            ;
        } else {
            Validation::view_errorcourseview_page();
            die();
        }
    }
} else {
    Session::destroy();
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    $(document).ready(function() {
    $('#users_table').DataTable();
} );
</script>