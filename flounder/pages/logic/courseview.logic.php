<?php
$database = DatabaseFactory::getFactory()->getConnection();
if(isset($_GET["course_id"]) && !empty($_SESSION["user"]["role_type_id"])) {

    //Define the course_id by get!
    $course_id = $_GET["course_id"];

    //Define the user_session_id
    $user_session_id = $_SESSION["user"]["id"];

    //This will return everything from the course
    $course = Createcourse::select_everything_from_course_by_course_id($course_id);
    
    //This will return the user information and the course information
    $user = Createcourse::select_courses_where_user_id($course_id, $user_session_id);

    //Check the return value
    if(($course || $user) === (false || null)) {
        Session::destroy();
        Validation::view_errorcourseview_page();
        die();
    } 

    function view_welcoming_message($user, $course) {
        if($user) {
            echo "Hallo " .$user["firstname"]." Welcome To The ".$course["course_name"]." Course";
        } else {
            if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
                echo "Hallo Admin " .$_SESSION["user"]["firstname"]." Welcome To The ".$course["course_name"]." Course";
            } else if(isset($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 2) {
                echo "Hallo Teacher " .$_SESSION["user"]["firstname"]." Welcome To The ".$course["course_name"]." Course";
            } else {
                Validation::view_errorcourseview_page();
                die();
            }
        }
    }
}
?>