<?php
$database = DatabaseFactory::getFactory()->getConnection();
$showFormular = true;

if(!empty($_POST["email_login"]) && !empty($_POST["password_login"])){

    $error = false;
    $email = $_POST["email_login"];
    $password = $_POST["password_login"];    

    $password_error = Validation::check_password_if_empty($password);
    $email_error = Validation::check_email_filter($email);

    if($password_error || $email_error){
        $error = true;
    }

    if(!$error){
        $get_user_session = Login::get_user_session($email, $password);
        if($get_user_session) {
            //die will set the $showFormular = false; and will send me to the checksessions file
            Validation::view_checksessions_page();
            die();
        } else {
            $showFormular = true;
        }
    }
}
?>