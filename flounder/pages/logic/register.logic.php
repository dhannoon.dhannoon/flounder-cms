<?php
$database = DatabaseFactory::getFactory()->getConnection();
//If user session is set the register will not be seen! for more security (sql injection)
if(!isset($_SESSION["user"]["id"])) {
    $showFormular = true;
    if(!empty($_POST["firstname"]) && (!empty($_POST["lastname"])) && (!empty($_POST["email"])) && (!empty($_POST["date_of_birth"])) && (!empty($_POST["password"])) && (!empty($_POST["confirmpassword"]))) {
        $error = false;
        //Define the post into variables
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $email = $_POST["email"];
        $date_of_birth = $_POST["date_of_birth"];
        $password = $_POST["password"];
        $confirmpassword = $_POST["confirmpassword"];
        $student_role_type = 3;

        //Calling this function to check if the entered password has an error
        $password_error = Validation::check_password_with_confirmpassword($password, $confirmpassword);

        //Calling this function to check if the entered email has an error
        $email_error = Validation::check_email_validate($email);
        
        //This if will check for me the returned variables if they are true.
        if($password_error || $email_error) {
            $error = true;
        }

        //If the email and the password funtions are passed the create_users function will be called
        if(!$error) {
            $register_user_and_get_session = Createusers::create_users($firstname, $lastname, $email, $password, $date_of_birth, $student_role_type);
            if($register_user_and_get_session === true) {
                //If the create_users function return true the login class with get_user_session will be called to give the user the session and send to the profile site
                $get_session = Login::get_user_session($email, $password);
                if($get_session) {
                    echo '<script> alert("Register did work"); </script>';
                    Validation::view_profile_page();
                    die();
                } else {
                    return false;
                    echo '<script> alert("Unfortunately, an error occurred while geting the session!")</script>';
                }
            } else {
                $showFormular = true;
            }
        } else {
            echo '<script> alert("Register did not work!")</script>';
        }
    }
} else {
    echo '<script> alert("You are already registered"); </script>';
    Validation::view_checksessions_page();
    die();
}
?>