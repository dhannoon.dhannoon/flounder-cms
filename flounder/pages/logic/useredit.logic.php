<?php
if(isset($_GET["user_id"]) && !empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {

    //Define the user_id from the get into a variable
    $user_id_to_edit = $_GET["user_id"];
    
    //Define the admin sessionID
    $admin_id = $_SESSION["user"]["id"];

    //Select everything from the user where the session(user_id) is in the get function!
    $user = Profile::select_everything_from_user($user_id_to_edit);

    //Check if the get user are in the database //if the select_everything_from_user function return null then i will view the users page!
    if($user === null) {
        echo '<script> alert("User did not be found!");</script>';
        Validation::view_users_page();
        die();
    }

    //This if will update the user information by the admin
    if(isset($_POST["firstname_update_by_admin"]) || isset($_POST["lastname_update_by_admin"]) || isset($_POST["date_of_birth_update_by_admin"]) || isset($_POST["phone_update_by_admin"]) || (isset($_POST["country_update_by_admin"])) || (isset($_FILES["uploadimage_update_by_admin"]["name"])) || (isset($_POST["role_type_id_by_admin"]))) {

        $firstname_update = $_POST["firstname_update_by_admin"];
        $lastname_update = $_POST["lastname_update_by_admin"];
        $date_of_birth_update = $_POST["date_of_birth_update_by_admin"];
        $phone_update = $_POST["phone_update_by_admin"];
        $country_update = $_POST["country_update_by_admin"];
        $role_type_id_update = $_POST["role_type_id_by_admin"];

        //needed for the user image upload by the admin
        $files_name = $_FILES["uploadimage_update_by_admin"]["name"];
        $files_tmp_name = $_FILES["uploadimage_update_by_admin"]["tmp_name"];
        $image_users_path = __DIR__."/../image/image_users/";
        $update_user_image_by_admin = Profile::upload_an_image_and_save_into_user_session_id($files_name, $files_tmp_name, $image_users_path, $user, $user_id_to_edit);
        
        //Call the update_user_information_by_admin function and add the post parms to the function, will update the user information
        $update_user = Useredit::update_user_information_by_admin($firstname_update, $lastname_update, $date_of_birth_update, $phone_update, $country_update, $role_type_id_update, $user_id_to_edit);
        if($update_user !== null) {
            echo '<script> alert("Updating the user information by the admin did not work!"); window.location = window.location.href;</script>';
        } else {
            echo '<script type="text/javascript"> alert("You did update the user with the email address: '.$user["email"].'"); window.location = window.location.href; </script>';
        }
    }

    //This function will change the user email and check if the email has been used before! Admin password will required to submit
    if(isset($_POST["user_email_update"]) && isset($_POST["confirm_user_email_update"]) && isset($_POST["admin_password_update_user_email"])) {
        
        $error = false;
        $user_email_update = $_POST["user_email_update"];
        $confirm_user_email_update = $_POST["confirm_user_email_update"];
        $admin_password_update_user_email = $_POST["admin_password_update_user_email"];
        
        $Profile_class = new Profile();
        $change_email_by_admin = $Profile_class->update_email($user_email_update, $confirm_user_email_update, $admin_password_update_user_email, $admin_id, $user_id_to_edit);
        if($change_email_by_admin === false) {
            echo '<script> alert("You did change the user E-Mail"); window.location = window.location.href;</script>';
        } else if($change_email_by_admin === true) {
            echo '<script> alert("E-Mail changing by admin did not work!"); window.location = window.location.href;</script>';
        }
    }

    //This if will update the password after I got the post var from the form
    if(isset($_POST["admin_password"]) && isset($_POST["user_new_password_update"]) && isset($_POST["confirm_user_new_password_update"])) {
        $error = false;
        $admin_password = $_POST["admin_password"];
        $user_new_password_update = $_POST["user_new_password_update"];
        $confirm_user_new_password_update = $_POST["confirm_user_new_password_update"];
        
        $Profile_class = new Profile();
        $update_user_password = $Profile_class->update_password($admin_password, $user_new_password_update, $confirm_user_new_password_update, $admin_id, $user_id_to_edit);
        if($update_user_password === false) {
            echo '<script> alert("You did change the user Password!"); window.location = window.location.href;</script>';
        } else if($update_user_password === true) {
            echo '<script> alert("Password changing by admin did not work!"); window.location = window.location.href;</script>';
        }
    }

    //This will remove the user by the admin
    if(isset($_POST["remove_account_by_admin"])) {
        $remove_user = Profile::remove_user($user_id_to_edit);
        if($remove_user === null) {
            Validation::view_users_page();
            die();
        }
        if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
            Validation::view_users_page();
            die();
        } else {
            Session::destroy();
            Validation::view_checksessions_page();
            die();
        }
    }
} else{
    Session::destroy();
    Validation::view_checksessions_page();
    die();
}
?>