<?php
$database = DatabaseFactory::getFactory()->getConnection();
if(isset($_SESSION["user"]["id"])) {

    $user_session_id = $_SESSION["user"]["id"];

    //This variable $user will echo the needed data in the profile view
    $user = Profile::select_everything_from_user($user_session_id);

    //After submitting the user information form
    if(isset($_POST["firstname_update"]) || isset($_POST["lastname_update"]) || isset($_POST["date_of_birth_update"]) || isset($_POST["phone_update"]) || (isset($_POST["country_update"])) || (isset($_FILES["uploadimage_update"]["name"]))) {
        $firstname_update = $_POST["firstname_update"];
        $lastname_update = $_POST["lastname_update"];
        $date_of_birth_update = $_POST["date_of_birth_update"];
        $phone_update = $_POST["phone_update"];
        $country_update = $_POST["country_update"];

        //needed for the image upload
        $files_name = $_FILES["uploadimage_update"]["name"];
        $files_tmp_name = $_FILES["uploadimage_update"]["tmp_name"];
        $image_users_path = __DIR__."/../image/image_users/";
        $update_user_image = Profile::upload_an_image_and_save_into_user_session_id($files_name, $files_tmp_name, $image_users_path, $user, $user_session_id);

        //Call the update_user_information function and add the post parms in the function
        $update_user_information = Profile::update_user_information($firstname_update, $lastname_update, $date_of_birth_update, $phone_update, $country_update, $user_session_id);
        if($update_user_information !== null) {
            echo '<script> alert("Updating the user information did not work!"); window.location = window.location.href;</script>';
        } else {
            echo '<script> window.location = window.location.href; </script>';
        }
    }

    //This if will update the email after I got the post var from the form
    if(isset($_POST["email_update"]) && isset($_POST["confirmemail_update"]) && isset($_POST["password_update_email"])) {
        $error = false;
        $email_update = $_POST["email_update"];
        $confirmemail_update = $_POST["confirmemail_update"];
        $password_update_email = $_POST["password_update_email"];

        $Profile_class = new Profile();
        $change_email = $Profile_class->update_email($email_update, $confirmemail_update, $password_update_email, $user_session_id, $user_session_id);
        if($change_email === false) {
            echo '<script> alert("You did change your E-Mail!"); window.location = window.location.href;</script>';
        } else if($change_email === true) {
            echo '<script> alert("E-Mail changing did not work!"); window.location = window.location.href;</script>';
        }
    }

    //This if will update the password after I got the post var from the form
    if(isset($_POST["password"]) && isset($_POST["password_update"]) && isset($_POST["confirmpassword_update"])) {
        $error = false;
        $old_password = $_POST["password"];
        $password_update = $_POST["password_update"];
        $confirmpassword_update = $_POST["confirmpassword_update"];
        
        $Profile_class = new Profile();
        $update_password = $Profile_class->update_password($old_password, $password_update, $confirmpassword_update, $user_session_id, $user_session_id);
        if($update_password === false) {
            echo '<script> alert("You did change your Password!"); window.location = window.location.href;</script>';
        } else if($update_password === true) {
            echo '<script> alert("Password changing did not work!"); window.location = window.location.href;</script>';
        }
    }

    //This will remove the user
    if(isset($_POST["remove_account"])) {
        $remove_user = Profile::remove_user($user_session_id);
        Session::destroy();
        echo '<script> alert("You did remove your account!"); window.location = window.location.href;</script>';
        if(!isset($user_session_id)) {
            Validation::view_checksessions_page();
            die();
        }
    }
} else {
    Validation::view_checksessions_page();
    die();
}
?>