<?php
$pdf_created_at = date("d.m.Y");

$pdf_header = '
Flounder
Course Management System
Admin
admin@admin.com
http://flounder';


$pdf_footer = "Hier is the pdf Footer";

$pdfName = "Users_into_".$course["course_name"]."_course_created_at_".$pdf_created_at.".pdf";


//////////////////////////// Inhalt des PDFs als HTML-Code \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$html = '
        <table cellpadding="5" cellspacing="0" style="width: 100%; ">
            <tr>
                <td>'.nl2br(trim($pdf_header)).'</td>
                <td style="text-align: right">
                    Course Name: '.$course["course_name"].'<br>
                    Course Created At: '.$course["course_create_at"].'<br>
                    Course Start: '.$course["course_start"].'<br>
                    Course End: '.$course["course_end"].'<br>
                    Exam Date: '.$course["exam_date"].'<br>
                    Course Point: '.$course["point"].'<br>
                </td>
            </tr>
            <br><br>
            <tr>
                <td style="font-size:1.3em; font-weight: bold; text-align:center;">
                    <b>Users List In '.$course["course_name"].' Course</b>
                </td>
            </tr>
            <br>
        </table>
        <br><br><br>

        <table cellpadding="5" cellspacing="0" style="width: 100%; border-collapse: collapse; border: 1px solid black;" border="0">
            <tr style="background-color: #cccccc; padding:10px;">
                <td style="text-align: left; border: 1px solid black;">
                    <b>Number</b>
                </td>
                <td style="text-align: left; border: 1px solid black;">
                    <b>First Name</b>
                </td>
                <td style="text-align: left; border: 1px solid black;">
                    <b>Last name</b>
                </td>
                <td style="text-align: left; border: 1px solid black;">
                    <b>E-Mail</b>
                </td>
            </tr>
        ';
            
    
$row_users = 0;
$count_participant = count($participants);

foreach($participants as $participant) {
    $user_information = Profile::select_everything_from_user($participant);
    $row_users++;
    $html .= '
            <tr>
                <td style="text-align: left; border: 1px solid black;">'.$row_users.'</td>
                <td style="text-align: left; border: 1px solid black;">'.$user_information["firstname"].'</td>
                <td style="text-align: left; border: 1px solid black;">'.$user_information["lastname"].'</td>
                <td style="text-align: left; border: 1px solid black;">'.$user_information["email"].'</td>
            </tr>
            ';
}

$html .='
            <tr>
                <td colspan="3">
                    <b>Users:</b>
                    <b>'.$count_participant.' user are in this course</b>
                </td>
            </tr>
        </table>
        ';

$html .='
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        ';


$html .= nl2br($pdf_footer);



//////////////////////////// Erzeugung eures PDF Dokuments \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// TCPDF Library laden
require(__DIR__.'./../../tcpdf/tcpdf.php');

// Erstellung des PDF Dokuments
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Dokumenteninformationen
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($pdfAuthor);
$pdf->SetTitle('Course PDF '.$pdf_created_at);
$pdf->SetSubject('Course PDF '.$pdf_created_at);


// Header und Footer Informationen
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// Auswahl des Font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// Auswahl der MArgins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// Automatisches Autobreak der Seiten
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// Image Scale 
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// Schriftart
$pdf->SetFont('dejavusans', '', 10);

// Neue Seite
$pdf->AddPage();

// Fügt den HTML Code in das PDF Dokument ein
$pdf->writeHTML($html, true, false, true, false, '');

//Ausgabe der PDF

//Variante 1: PDF direkt an den Benutzer senden:
//$pdf->Output($pdfName, 'I');

//Variante 2: PDF im Verzeichnis abspeichern:
$pdf->Output(dirname(__FILE__).'/../pdf/'.$pdfName, 'F');
//echo 'PDF herunterladen: <a href="pages/pdf/'.$pdfName.'">'.$pdfName.'</a>';
?>