<?php
if(isset($_GET["course_id"]) && !empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {

    $database = DatabaseFactory::getFactory()->getConnection();

    //Define the course_id from the get into a variable
    $course_id_to_edit = $_GET["course_id"];
    
    //Define the admin sessionID
    $admin_id = $_SESSION["user"]["id"];

    $course = Createcourse::select_everything_from_course_by_course_id($course_id_to_edit);

    //If will return nothing form the database then the user will be send to errorcourseview
    if(!$course) {
        Validation::view_errorcourseview_page();
        die();
    }

    //This will send me to the current courseedit page with the opened course id
    $navigate_to_current_course = "?p=courseedit&course_id=".$course_id_to_edit;

    //Define the role type for student
    $student_role_type_id = 3;

    $users = Createcourse::select_all_users_by_role_type_id($student_role_type_id);


    //This here will remove all the users from the course
    if(!empty($_GET["delete_users"])) {
        //Define the deleted user
        $delete_users = $_GET["delete_users"];

        //Call the function delete_users_from_course to remove the users from course
        $delete_users = Createcourse::delete_users_from_course($course_id_to_edit);

        //Make sure that the user is removed! if there was an error return the course page
        if($delete_users !== true) {
            echo '<script> alert("Unfortunately, an error occurred while deleting the users")</script>';
            Validation::view_course_page();
            die();
        }
        //This will send me to the current courseedit page with the opened course id
        //Header("Location: ".$navigate_to_current_course);
        echo("<script>location.href = '.$navigate_to_current_course';</script>");
        die();
    }

    if(!empty($_POST["course_name"]) || !empty($_POST["course_start"]) || !empty($_POST["course_end"]) || !empty($_POST["exam_date"]) || !empty($_POST["point"]) || !empty($_POST["course_html"])) {
        $course_name = $_POST["course_name"];
        $course_start = $_POST["course_start"];
        $course_end = $_POST["course_end"];
        $exam_date = $_POST["exam_date"];
        $point = $_POST["point"];
        $course_html = $_POST["course_html"];

        //This will update my course information
        $update_course = Createcourse::update_course_id($course_name, $course_start, $course_end, $exam_date, $point, $course_html, $course_id_to_edit);
    }


    $select_user_id_from_course = Createcourse::select_user_id_from_course_participants_by_course_id($course_id_to_edit);

    $participants = [];
    
    //Here I will get the added user_id into course_id
    while ($participants_results = $select_user_id_from_course->fetch_assoc()) {
        //All the users are in the aparticipants array where the course_id is
        $participants[] = $participants_results["user_id"];
    }

    //If add_user_into_course POST is seset and it is not empty or empty then call the delete function or the 
    if(isset($_POST["add_user_into_course"]) && ((!empty($_POST["add_user_into_course"]) || $_POST["add_user_into_course"] === ""))){

        //Define the add_user_into_course into array
        $array_user = $_POST["add_user_into_course"];
        
        //Call the function delete_users_from_course to remove the users from course when the add_user_into_course is not empty or is null!
        $delete_user_from_course = Createcourse::delete_users_from_course($course_id_to_edit);
        
        //empty array 
        $array_insert_users=[];

        //In this if i check if the variable $array_user is an array or an object
        if(is_array($array_user) || is_object($array_user)) {
            foreach($array_user as $user_id){
                if(!in_array($user_id, $participants)){
                    //Getting the added user_id into course in array
                    $array_insert_users[] = $user_id;
                }
            }
            foreach($array_user AS $user_insert_into_database) {

                //This function will add the user into the course based on the user array
                $add_user_into_course = Createcourse::add_user_into_course($user_insert_into_database, $course_id_to_edit);
                if($add_user_into_course !== null) {
                    echo '<script> alert("Unfortunately, an error occurred while adding user into course!")</script>';
                    Validation::view_course_page();
                    die();
                }
            }
        }
    }
    //Hier I do call the pdf.logic file
    require(__DIR__.'/pdf.logic.php');
} else {
    Session::destroy();
    Validation::view_checksessions_page();
    die();
}
?>