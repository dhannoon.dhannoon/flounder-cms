<?php
$database = DatabaseFactory::getFactory()->getConnection();

//Visit this if only if you are the admin or the teacher!
if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {

    // <input type="text" value="hallowelt" name="myPostName" style="display:none;">
    // <input type="text" value="hallowelt" name="myPostName" hidden>
    if(isset($_POST["insert_course_id"]) && !empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
        $course_id_to_insert = $_POST["insert_course_id"];
        $user_id = $_SESSION["user"]["id"];
    
        if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
            $verify = 1;
            $register_user_to_course = Createcourse::add_user_into_verify_course_table($user_id, $course_id_to_insert, $verify);
            
            if($register_user_to_course !== true) {
                echo '<script> alert("Unfortunately, an error occurred while add you into the waiting list, to be verified from the admin!")</script>';
                Validation::view_registerforcourses_page();
                die();
            }
            if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
                echo '<script> alert("You have to wait till the admin verify your request!")</script>';
                Validation::view_registerforcourses_page();
                die();
            } else {
                Session::destroy();
                Validation::view_checksessions_page();
                die();
            }
        } else {
            Session::destroy();
            Validation::view_checksessions_page();
            die();
        }
    }

    //This function will select everything from course
    $all_information_from_course_table = Createcourse::select_everything_from_course();

    //This function will view all the courses for the admin and the teacher!
    function view_all_courses_for_student_to_register($all_information_from_course_table)
    {

        //Check if the returned result (num_rows) are zero
        if ($all_information_from_course_table->num_rows == 0) {
            echo '<script> alert("There are no courses to view!")</script>';
            echo "Course site is empty.";
            die();
        }

        //If the returned (all_information_from_course_table) true then -> fetch
        if ($all_information_from_course_table) {
            echo " <table id='registerforcourses_table' class='table table-striped table-bordered' style='width:100%;'>
                        <thead>
                            <tr>
                                <th align='left'>Number</th>
                                <th align='left'>Course Name</th>
                                <th align='left'>Course Start</th>
                                <th align='left'>Course End</th>
                                <th align='left'>Exam Date</th>
                                <th align='left'>Course Point</th>
                                <th align='left'>Registration</th>
                            </tr>
                        </thead>
                    <tbody>
            ";
            $row_counter_courses_available = 0;
            //Getting the course information in array!
            while ($course = $all_information_from_course_table->fetch_assoc()) {

                //If the fetch was true then
                if ($course) {
                    echo " 
                            <tr style='padding: 8px;'>
                                <td>";
                    $row_counter_courses_available++;
                    echo $row_counter_courses_available;
                    echo        "</td>
                                <td>".$course["course_name"]."</td>
                                <td>".$course["course_start"]."</td>
                                <td>".$course["course_end"]."</td>
                                <td>".$course["exam_date"]."</td>
                                <td>".$course["point"]."</td>
                                <td>";
                               $is_verify =  Createcourse::select_verify_where_course_id_and_user_id($_SESSION["user"]["id"], $course["id"]);
                               //echo $is_verify["verify"];
                               if($is_verify["verify"] === 1) {

                                echo "You did Send a Registration Request for this course";
                                } else if($is_verify["verify"] === 0) {

                                    echo "The admin did not verify your Request!";
                                } else if($is_verify["verify"] === 2) {

                                    echo "The admin did verify your Request, Go to courses page to see the course!";
                                } else {
                                    ?>
                                    <form action="" method="post">
                                        <input type="hidden" name="insert_course_id" value="<?php echo $course["id"]; ?>">
                                        <button type="submit">Register For <?php echo $course["course_name"]; ?> Course</button>
                                    </form>
                                    <?php
                                }
                    echo        "</td>
                            </tr>
                       ";
                } else {
                    Session::destroy();
                    echo '<script> alert("Unfortunately, an error occurred while getting the course information from the database")</script>';
                    Validation::view_checksessions_page();
                    die();
                }
            }
            echo "  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th align='left'>Number</th>
                            <th align='left'>Course Name</th>
                            <th align='left'>Course Start</th>
                            <th align='left'>Course End</th>
                            <th align='left'>Exam Date</th>
                            <th align='left'>Course Point</th>
                            <th align='left'>Registration</th>
                        </tr>
                    </tfoot>
                </table>"
            ;
        } else {
            Validation::view_errorcourseview_page();
            die();
        }
    }
} else {
    //If this page will be called from outside and has not the admin session then will send to checksessions page!
    Session::destroy();
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    $(document).ready(function() {
    $('#registerforcourses_table').DataTable();
} );
</script>