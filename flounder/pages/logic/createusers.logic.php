<?php
$database = DatabaseFactory::getFactory()->getConnection();
$showFormular = true;

if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
    if(!empty($_POST["firstname_create"]) && (!empty($_POST["lastname_create"])) && (!empty($_POST["email_create"])) && (!empty($_POST["date_of_birth_create"])) && (!empty($_POST["password_create"])) && (!empty($_POST["confirmpassword_create"])) && (!empty($_POST["role_type_create"]))) {
        $error = false;
        $firstname = $_POST["firstname_create"];
        $lastname = $_POST["lastname_create"];
        $email = $_POST["email_create"];
        $date_of_birth = $_POST["date_of_birth_create"];
        $password = $_POST["password_create"];
        $confirmpassword = $_POST["confirmpassword_create"];
        $role_type_id = $_POST["role_type_create"];
        
        //Calling this function to check if the entered password has an error
        $password_error = Validation::check_password_with_confirmpassword($password, $confirmpassword);

        //Calling this function to check if the entered email has an error
        $email_error = Validation::check_email_validate($email);
        
        //This if will check for me the returned variables if they are true.
        if($password_error || $email_error){
            $error = true;
        }

        //If the returned variables are not true error then the funtion create_users_by_admin will be called
        if(!$error) {
            $create_user_and_get_session = Createusers::create_users($firstname, $lastname, $email, $password, $date_of_birth, $role_type_id);
            if($create_user_and_get_session === true) {
                $last_insert_id = $database->insert_id;
                if($last_insert_id) {
                    $navigate_to_last_created_user = "?p=useredit&user_id=".$last_insert_id;
                    echo("<script>location.href = '.$navigate_to_last_created_user';</script>");
                    die();
                } else {
                    return false;
                    echo '<script> alert("Unfortunately, an error occurred while saving")</script>';
                }
            } else {
                $showFormular = true;
            }
        } else {
            echo '<script> alert("Creating user did not work!")</script>';
        }
    }
} else {
    echo '<script> alert("You are not the admin!")</script>';
    $showFormular = false;
    Validation::view_checksessions_page();
    die();
}
?>