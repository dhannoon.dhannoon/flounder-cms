<?php
$database = DatabaseFactory::getFactory()->getConnection();
$showFormular = true;

if(!empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {
    if(!empty($_POST["course_name"]) && !empty($_POST["course_start"]) && !empty($_POST["course_end"]) && !empty($_POST["exam_date"]) && !empty($_POST["course_point"])) {

        $course_name = $_POST["course_name"];
        $course_start = $_POST["course_start"];
        $course_end = $_POST["course_end"];
        $exam_date = $_POST["exam_date"];
        $course_point = $_POST["course_point"];

        //Define the user session id
        $user_session_id = $_SESSION["user"]["id"];

        //This function will create the course with the given Parameter
        $create_course = Createcourse::create_course($course_name, $course_start, $course_end, $exam_date, $course_point, $user_session_id);

        if($create_course === true) {
            $last_insert_id = $database->insert_id;
            if($last_insert_id) {
                //This function will select for me all the assigned courses by the user session id
                $course = Createcourse::select_everything_from_course_by_user_id($user_session_id);
                if($course) {
                    $showFormular = false;
                    //die(header('Location: ?p=courseview&course_id='.$last_insert_id));
                    $navigate_to_last_created_course = "?p=courseview&course_id=".$last_insert_id;
                    echo("<script>location.href = '.$navigate_to_last_created_course';</script>");
                    die();
                } else {
                    echo '<script> alert("Unfortunately, an error occurred while selecting the course")</script>';
                    Validation::view_course_page();
                    die();
                }
            } else {
                echo '<script> alert("Unfortunately, an error occurred while saving the course")</script>';
                Validation::view_course_page();
                die();
            }
        } else {
            echo '<script> alert("Unfortunately, an error occurred while creating the course!")</script>';
            Validation::view_course_page();
            die();
        }
    }
}
?>