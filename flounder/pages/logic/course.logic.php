<?php
$database = DatabaseFactory::getFactory()->getConnection();

// Visit this side if you are the admin or the teacher!
if(!empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {


    if(isset($_POST["coursedelete_where_course_id"]) && (!empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2))) {

        //Define the course_id from GET
        $course_id_to_delete = $_POST["coursedelete_where_course_id"];
    
        //This will remove the user by the admin
        if(!empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {
            $delete_course = Createcourse::remove_course_by_admin($course_id_to_delete);
            if($delete_course === null) {
                Validation::view_course_page();
                die();
            }
            if(!empty($_SESSION["user"]["role_type_id"]) && ($_SESSION["user"]["role_type_id"] === 1 || $_SESSION["user"]["role_type_id"] === 2)) {
                Validation::view_course_page();
                die();
            } else {
                Session::destroy();
                Validation::view_checksessions_page();
                die();
            }
        } else {
            Session::destroy();
        }
    }

    //This function will select everything from course
    $all_from_course_table = Createcourse::select_everything_from_course();

    //This function will view all the courses for the admin and the teacher!
    function view_all_courses_for_admin($all_from_course_table) {

        //Check if the returned result (num_rows) are zero
        if($all_from_course_table->num_rows == 0) {
            echo '<script> alert("There are no courses to view!");</script>';
            echo "Course site is empty. You have to create course at first and then visit this page";
            die();
        }

        //If the returned (all_from_course_table) true then -> fetch
        if($all_from_course_table) {
            echo " <table id='courses_table' class='table table-striped table-bordered' style='width:100%;'>
                        <thead>
                            <tr>
                                <th align='left'>Number</th>
                                <th align='left'>Course Name</th>
                                <th align='left'>Course Start</th>
                                <th align='left'>Course End</th>
                                <th align='left'>Exam Date</th>
                                <th align='left'>Course Point</th>
                                <th align='left'>View</th>
                                <th align='left'>Edit</th>
                                <th align='left'>Delete</th>
                            </tr>
                        </thead>
                    <tbody>
            ";
            $row_counter_course_admin = 0;
            //Getting the course information in array!
            while($course = $all_from_course_table->fetch_assoc()) {

                //If the fetch was true then
                if($course) {
                    echo " 
                            <tr style='padding: 8px;'>
                                <td>";
                                $row_counter_course_admin++;
                                echo $row_counter_course_admin;
                    echo        "</td>
                                <td>".$course["course_name"]."</td>
                                <td>".$course["course_start"]."</td>
                                <td>".$course["course_end"]."</td>
                                <td>".$course["exam_date"]."</td>
                                <td>".$course["point"]."</td>
                                <td><a href='index.php?p=courseview&course_id=".$course["id"]."'>View ".$course["course_name"]." Course</a></td>
                                <td><a href='index.php?p=courseedit&course_id=".$course["id"]."'>Edit ".$course["course_name"]." Course</a></td>
                                <td>";
                                    ?>
                                        <form action="" method="post">
                                            <input type="hidden" name="coursedelete_where_course_id" value="<?php echo $course["id"]; ?>">
                                            <button type="submit">Delete <?php echo $course["course_name"]; ?> Course</button>
                                        </form>
                                    <?php
                    echo        "</td>
                            </tr>
                       ";
                } else {
                    Session::destroy();
                    echo '<script> alert("Unfortunately, an error occurred while getting the course information from the database")</script>';
                    Validation::view_checksessions_page();
                    die();
                }
            }
            echo "  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th align='left'>Number</th>
                            <th align='left'>Course Name</th>
                            <th align='left'>Course Start</th>
                            <th align='left'>Course End</th>
                            <th align='left'>Exam Date</th>
                            <th align='left'>Course Point</th>
                            <th align='left'>View</th>
                            <th align='left'>Edit</th>
                            <th align='left'>Delete</th>
                        </tr>
                    </tfoot>
                </table>"
            ;
        } else {
            Validation::view_errorcourseview_page();
            die();
        }
    }

// Visit this side if you are role_type student is!
} else if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {

    if(isset($_POST["deregister_course_id"]) && !empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {

        $deregister_course_id = $_POST["deregister_course_id"];
        $user_id = $_SESSION["user"]["id"];
    
        if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
    
            $deregister_user_for_course = Createcourse::delete_users_from_course_where_course_id_and_user_id($deregister_course_id, $user_id);
    
            if($deregister_user_for_course !== true) {
                echo '<script> alert("Unfortunately, an error occurred while deregistering you from course!");</script>';
                Validation::view_course_page();
                die();
            }
            if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 3) {
                echo '<script> alert("Deregistering you from course did work!");</script>';
                Validation::view_course_page();
                die();
            } else {
                Session::destroy();
                Validation::view_checksessions_page();
                die();
            }
        } else {
            Session::destroy();
            Validation::view_checksessions_page();
            die();
        }
        //Do not verify users to courses
    }

    //Define the user session id
    $user_id = $_SESSION["user"]["id"];

    //This function will select everything from the course_participants by the user session id
    $student_are_assigned_into_course_id = Createcourse::select_all_from_course_participants_by_user_id($user_id);

    //This function is for viewing the courses where the student assigned
    function student_in_course($student_are_assigned_into_course_id) {

        //Check if the returned result (num_rows) are zero
        if($student_are_assigned_into_course_id->num_rows == 0) {
            //echo "Something happened while getting the courses!";
            echo '<script> alert("Unfortunately, you did not sign in for any course yet!");</script>';
            Validation::view_errorcourseview_page();
            die();
        }

        //If the returned Par. is true then fetch! if not send student to errorcourseview page
        if ($student_are_assigned_into_course_id) {

            echo " <table id='courses_table' class='table table-striped table-bordered' style='width:100%;'>
                        <thead>
                            <tr>
                                <th align='left'>Number</th>
                                <th align='left'>Course Name</th>
                                <th align='left'>Course Start</th>
                                <th align='left'>Course End</th>
                                <th align='left'>Exam Date</th>
                                <th align='left'>Course Point</th>
                                <th align='left'>View</th>
                                <th align='left'>Deregister</th>
                            </tr>
                        </thead>
                    <tbody>
            ";
            $row_counter_course_student = 0;
            //The var student_assigned_in_course is an array with all the information from table course_participants
            while ($student_assigned_in_course = $student_are_assigned_into_course_id->fetch_assoc()) {

                //Get the course_id from the table!
                $course_id = $student_assigned_in_course["course_id"];

                //This function will select for me everything from table course where course_id is
                $courses = Createcourse::select_everything_from_course_by_course_id_without_fetch($course_id);

                //Check if the returned result (num_rows) are zero(where the courses are)
                if($courses->num_rows == 0) {
                    Session::destroy();
                    echo '<script> alert("Unfortunately, Something happened while getting the courses!")</script>';
                    Validation::view_checksessions_page();
                    die();
                }

                //If getting the courses was true then fetch
                if($courses) {

                    //Getting the course information in array!
                    while($course = $courses->fetch_assoc()) {

                        //If the fetch was true then
                        if($course) {
                            echo "
                                    <tr style='padding: 8px;'>
                                        <td>";
                                        $row_counter_course_student++;
                                        echo $row_counter_course_student;
                            echo        "</td>
                                        <td>".$course["course_name"]."</td>
                                        <td>".$course["course_start"]."</td>
                                        <td>".$course["course_end"]."</td>
                                        <td>".$course["exam_date"]."</td>
                                        <td>".$course["point"]."</td>
                                        <td><a href='index.php?p=courseview&course_id=".$course["id"]."'>View ".$course["course_name"]." Course</a></td>
                                        <td>";
                                            ?>
                                            <form action="" method="post">
                                                <input type="hidden" name="deregister_course_id" value="<?php echo $course["id"]; ?>">
                                                <button type="submit">Deregister form <?php echo $course["course_name"]; ?> Course</button>
                                            </form>
                                            <?php
                            echo        "</td>
                                    </tr>
                            ";
                        } else {
                            Session::destroy();
                            echo '<script> alert("Unfortunately, an error occurred while getting the course information from the database")</script>';
                            Validation::view_checksessions_page();
                            die();
                        }
                    }
                }
            }
            echo "  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th align='left'>Number</th>
                            <th align='left'>Course Name</th>
                            <th align='left'>Course Start</th>
                            <th align='left'>Course End</th>
                            <th align='left'>Exam Date</th>
                            <th align='left'>Course Point</th>
                            <th align='left'>View</th>
                            <th align='left'>Deregister</th>
                        </tr>
                    </tfoot>
                </table>"
            ;
        } else {
            Validation::view_errorcourseview_page();
            die();
        }
    }
    //send user to checksessions page if did not login!
} else {
    Session::destroy();
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    $(document).ready(function() {
    $('#courses_table').DataTable();
} );
</script>