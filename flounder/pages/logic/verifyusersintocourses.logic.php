<?php
$database = DatabaseFactory::getFactory()->getConnection();

//Visit this if only if you are the admin or the teacher!
if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {

    if(isset($_POST["verify_users_into_courses"]) && isset($_POST["verify_user_id"]) && isset($_POST["verify_course_id"]) && !empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {

        $verify_users_into_courses = $_POST["verify_users_into_courses"];
        $verify_user_id = $_POST["verify_user_id"];
        $verify_course_id = $_POST["verify_course_id"];
    
        if($verify_users_into_courses == 0) {
            if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
    
                $set_verify_to_zero = Createcourse::update_user_into_verify_course_table_where_userid_and_course_id($verify_users_into_courses, $verify_user_id, $verify_course_id);
        
                if($set_verify_to_zero !== true) {
                    echo '<script> alert("Unfortunately, an error occurred while trying to not verify the user to course!");</script>';
                    Validation::view_verifyusersintocourses_page();
                    die();
                }
                if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
                    echo '<script> alert("Your submit for not verifying the user to course has been done!");</script>';
                    Validation::view_verifyusersintocourses_page();
                    die();
                } else {
                    Session::destroy();
                    Validation::view_checksessions_page();
                    die();
                }
            } else {
                Session::destroy();
                Validation::view_checksessions_page();
                die();
            }
        } else if($verify_users_into_courses == 2) {
            if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
    
                $set_verify_to_two = Createcourse::update_user_into_verify_course_table_where_userid_and_course_id($verify_users_into_courses, $verify_user_id, $verify_course_id);
        
                if($set_verify_to_two !== true) {
                    echo '<script> alert("Unfortunately, an error occurred while trying to verifying the user into course!");</script>';
                    Validation::view_verifyusersintocourses_page();
                    die();
                }
                //insert the user_id and course_id into the course_participants table
                $verifying_user_into_course = Createcourse::add_user_into_course($verify_user_id, $verify_course_id);
                if($verifying_user_into_course !== null) {
                    echo '<script> alert("Unfortunately, an error occurred while add the user into course!");</script>';
                    Validation::view_verifyusersintocourses_page();
                    die();
                }
                if(!empty($_SESSION["user"]["role_type_id"]) && $_SESSION["user"]["role_type_id"] === 1) {
                    echo '<script> alert("Your submit for verifying the user to course has been done!");</script>';
                    Validation::view_verifyusersintocourses_page();
                    die();
                } else {
                    Session::destroy();
                    Validation::view_checksessions_page();
                    die();
                }
            } else {
                Session::destroy();
                Validation::view_checksessions_page();
                die();
            }
        } else {
            Session::destroy();
            Validation::view_checksessions_page();
            die();
        }
    }

    //This function will select everything from course
    $waiting_to_verify = 1;
    $select_everything_from_verify_users_into_course_table = Createcourse::select_everything_from_verify_users_into_courses($waiting_to_verify);

    //This function will view all the courses for the admin and the teacher!
    function view_all_users_for_the_admin_to_verify($select_everything_from_verify_users_into_course_table)
    {

        //Check if the returned result (num_rows) are zero
        if ($select_everything_from_verify_users_into_course_table->num_rows == 0) {
            echo '<script> alert("There are no users to verify into courses!");</script>';
            die();
        }

        //If the returned (all_information_from_course_table) true then -> fetch
        if ($select_everything_from_verify_users_into_course_table) {
            echo " <table id='verifyusersintocourses_table' class='table table-striped table-bordered' style='width:100%;'>
                        <thead>
                            <tr>
                                <th align='left'>Number</th>
                                <th align='left'>Course Name</th>
                                <th align='left'>User name</th>
                                <th align='left'>User E-Mail</th>
                                <th align='left'>Verify User Into Course</th>
                                <th align='left'>Do Not Verify User Request</th>
                            </tr>
                        </thead>
                    <tbody>
            ";
            $row_counter_verify_users_into_courses = 0;
            //Getting the course information in array!
            while ($verify_users_into_courses = $select_everything_from_verify_users_into_course_table->fetch_assoc()) {

                //If the fetch was true 
                $do_not_verify = 0;
                $do_verify = 2;
                if ($verify_users_into_courses) {
                    echo " 
                            <tr style='padding: 8px;'>
                                <td>";
                    $row_counter_verify_users_into_courses++;
                    echo $row_counter_verify_users_into_courses;
                    echo        "</td>
                                <td>";
                                $course_name = Createcourse::select_everything_from_course_by_course_id($verify_users_into_courses["course_id"]);
                                echo $course_name["course_name"];
                    echo        "</td>
                                <td>";
                                $user_information = Profile::select_everything_from_user($verify_users_into_courses["user_id"]);
                                echo $user_information["firstname"];
                    echo        "</td>
                                <td>".$user_information["email"]."</td>
                                <td>";
                                //Do verify form
                                    ?>
                                        <form action="" method="post">
                                            <input type="hidden" name="verify_users_into_courses" value="<?php echo $do_verify; ?>">
                                            <input type="hidden" name="verify_user_id" value="<?php echo $verify_users_into_courses["user_id"]; ?>">
                                            <input type="hidden" name="verify_course_id" value="<?php echo $verify_users_into_courses["course_id"]; ?>">
                                            <button type="submit">Do Verify</button>
                                        </form>
                                    <?php
                    echo        "</td>
                                <td>";
                                //Do not verify form
                                    ?>
                                        <form action="" method="post">
                                            <input type="hidden" name="verify_users_into_courses" value="<?php echo $do_not_verify; ?>">
                                            <input type="hidden" name="verify_user_id" value="<?php echo $verify_users_into_courses["user_id"]; ?>">
                                            <input type="hidden" name="verify_course_id" value="<?php echo $verify_users_into_courses["course_id"]; ?>">
                                            <button type="submit">Do not Verify</button>
                                        </form>
                                    <?php
                    echo        "</td>
                            </tr>
                       ";
                } else {
                    Session::destroy();
                    echo '<script> alert("Unfortunately, an error occurred while getting the course information from the database");</script>';
                    Validation::view_checksessions_page();
                    die();
                }
            }
            echo "  
                    </tbody>
                    <tfoot>
                        <tr>
                        <th align='left'>Number</th>
                        <th align='left'>Course Name</th>
                        <th align='left'>User name</th>
                        <th align='left'>User E-Mail</th>
                        <th align='left'>Verify User Into Course</th>
                        <th align='left'>Do Not Verify User Reques</th>
                        </tr>
                    </tfoot>
                </table>"
            ;
        } else {
            Validation::view_errorcourseview_page();
            die();
        }
    }
} else {
    Session::destroy();
    echo '<script> alert("You are not the admin!");</script>';
    Validation::view_checksessions_page();
    die();
}
?>
<script>
    $(document).ready(function() {
    $('#verifyusersintocourses_table').DataTable();
} );
</script>