<?php
require(__DIR__.'/logic/login.logic.php');
?>
<div class="w3-container">
    <!--<button onclick="document.getElementById('login').style.display='block'" style="width:auto;">Login</button>-->
    <div id="login">
        <?php
        # call the showFormular if it is false then do not show the form!
        if($showFormular) {
        ?>
        <form class="w3-modal-content animate" action="" method="post">
            <div class="imagecontainer">
                <!--<span onclick="document.getElementById('login').style.display='none'" class="close" title="close login">&times;</span>-->
                <img id="my_image" src="./pages/image/flounder.png" alt="logo" class="logo" width="300" height="250">
            </div>
            <div class="w3-container">
                <label for="email"><b>E-Mail</b></label>
                    <input type="email" class="w3-input-field" placeholder="Enter your E-Mail Address" id="email" name="email_login" require>
                <label for="password"><b>Password</b></label>
                    <input type="password" class="w3-input-field" placeholder="Enter your Password here" id="password" name="password_login" require>
                <button type="submit" class="button" id="login_button">Login</button>
                <label>
                    <input type="checkbox" checked="checked" id="remember" name="remember">Remember me
                </label>
            </div>
            <div class="w3-container footer">
                <button type="button" onclick="window.location.href = '?p=home';" class="cancelbtn">Cancel</button>
                <span class="password">
                    Forgot <a href="?p=resetpassword">password?</a>
                </span>
            </div>
            <div id="register" class="w3-container register">
                <p>Do not you have an account? <a href="?p=register">Register</a>.</p>
            </div>
        </form>
        <?php
        } //Ende from the if($showFormular)
        ?>
    </div>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('login');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "w3-topnav") {
            x.className += " responsive";
        } else {
            x.className = "w3-topnav";
        }
    }
</script>