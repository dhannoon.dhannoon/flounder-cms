#!/bin/sh

echo "Set docker images down"
docker ps
docker port mysql
docker port phpmyadmin
docker port flounder
pwd
sleep 2m
docker exec -i mysql mysql -uroot -proot flounder < init/flounder_empty_database.sql
echo "Passed with importing the database"
sleep 1m
docker-compose down