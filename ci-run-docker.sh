#!/bin/sh

echo "ci-run-docker.sh file has been called"

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer


# Install phpunit, the tool that we will use for testing


# Install mysql driver
# Here you can install any other extension that you need
# docker-php-ext-install pdo_mysql
# docker-php-ext-install mysqli
# docker-php-ext-enable

docker-compose up -d --build
#wait 60
#docker-compose up --abort-on-container-exit --exit-code-from web db phpmyadmin web


# curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit.phar"
# chmod +x /usr/local/bin/phpunit
# docker-compose down
# docker-compose up -d
# docker build

# docker-compose pull --ignore-pull-failures
# docker-compose up --abort-on-container-exit --exit-code-from flounder_db_1 flounder_app_1

