-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 10, 2021 at 03:25 PM
-- Server version: 8.0.22
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flounder`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int NOT NULL,
  `userid` int DEFAULT NULL,
  `course_create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `course_name` varchar(2222) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_start` date DEFAULT NULL,
  `course_end` date DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `point` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `document` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `course_html` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `userid`, `course_create_at`, `course_name`, `course_start`, `course_end`, `exam_date`, `point`, `document`, `course_html`) VALUES
(1, 1, '2021-01-10 15:09:22', 'German', '2021-02-10', '2021-02-20', '2021-02-25', '10', NULL, NULL),
(2, 1, '2021-01-10 15:10:10', 'Englisch', '2021-02-15', '2021-02-20', '2021-02-22', '10', NULL, NULL),
(3, 1, '2021-01-10 15:10:47', 'Informatik', '2021-02-13', '2021-02-23', '2021-02-26', '10', NULL, NULL),
(4, 1, '2021-01-10 15:11:22', 'Arabisch', '2021-02-10', '2021-02-20', '2021-02-25', '10', NULL, NULL),
(5, 3, '2021-01-10 15:24:45', 'ECDL', '2021-02-23', '2021-02-24', '2021-02-25', '10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course_material`
--

CREATE TABLE `course_material` (
  `id` int NOT NULL,
  `course_material_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `course_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_material_to_course`
--

CREATE TABLE `course_material_to_course` (
  `course_id` int NOT NULL,
  `course_material_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_participants`
--

CREATE TABLE `course_participants` (
  `user_id` int NOT NULL,
  `course_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_participants`
--

INSERT INTO `course_participants` (`user_id`, `course_id`) VALUES
(2, 1),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `role_type`
--

CREATE TABLE `role_type` (
  `id` int NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_type`
--

INSERT INTO `role_type` (`id`, `role_name`) VALUES
(1, 'admin'),
(2, 'teacher'),
(3, 'student');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_image` varchar(225) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_type_id` int DEFAULT NULL,
  `is_account_confirmed` tinyint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `date_of_birth`, `gender`, `profile_image`, `created_at`, `updated_at`, `last_seen`, `status`, `address`, `country`, `city`, `zip`, `phone`, `role_type_name`, `role_type_id`, `is_account_confirmed`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', '$2y$10$w9Jxfw3mIgWvIW83heaRgerp8jVd6vVitQFIa4NkgNLfGXOGf7LbG', '2021-01-01', NULL, NULL, '2021-01-03 15:33:31', NULL, '2021-01-03 15:33:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(2, 'Student', 'Student', 'student@student.com', '$2y$10$rBTKz4A5rdhELhe0klFmEeXGwVkQC3RuphzAO0FIEd5MW.o312TtW', '1997-09-23', NULL, NULL, '2021-01-10 15:13:46', NULL, '2021-01-10 15:13:46', NULL, NULL, 'Austria', NULL, NULL, '0660', NULL, 3, NULL),
(3, 'Teacher', 'Teacher', 'teacher@teacher.com', '$2y$10$k026QwQEJa3mzcMMqSqEJ.xvS9pBIiogYNqQ4X1VuhZkY1hXlt9.q', '1997-09-23', NULL, NULL, '2021-01-10 15:15:03', NULL, '2021-01-10 15:15:03', NULL, NULL, 'Austria', NULL, NULL, '', NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verify_users_into_courses`
--

CREATE TABLE `verify_users_into_courses` (
  `user_id` int NOT NULL,
  `course_id` int NOT NULL,
  `verify` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verify_users_into_courses`
--

INSERT INTO `verify_users_into_courses` (`user_id`, `course_id`, `verify`) VALUES
(2, 1, 2),
(2, 2, 0),
(2, 3, 2),
(2, 4, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_material`
--
ALTER TABLE `course_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_material_to_course`
--
ALTER TABLE `course_material_to_course`
  ADD PRIMARY KEY (`course_id`,`course_material_id`),
  ADD KEY `course_material_id` (`course_material_id`);

--
-- Indexes for table `course_participants`
--
ALTER TABLE `course_participants`
  ADD PRIMARY KEY (`user_id`,`course_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `role_type`
--
ALTER TABLE `role_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_type_id` (`role_type_id`);

--
-- Indexes for table `verify_users_into_courses`
--
ALTER TABLE `verify_users_into_courses`
  ADD PRIMARY KEY (`user_id`,`course_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `course_material`
--
ALTER TABLE `course_material`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_type`
--
ALTER TABLE `role_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_material_to_course`
--
ALTER TABLE `course_material_to_course`
  ADD CONSTRAINT `course_material_to_course_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_material_to_course_ibfk_2` FOREIGN KEY (`course_material_id`) REFERENCES `course_material` (`id`);

--
-- Constraints for table `course_participants`
--
ALTER TABLE `course_participants`
  ADD CONSTRAINT `course_participants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_participants_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_type_id`) REFERENCES `role_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
