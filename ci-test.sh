#!/bin/sh
#docker-compose up -d --build

docker-compose up -d --no-deps --build phpmyadmin mysql flounder

docker ps
docker port mysql
docker port phpmyadmin
docker port flounder
pwd
sleep 1m
docker exec -i mysql mysql -uroot -proot flounder < init/flounder_empty_database.sql

sleep 1m

docker-compose up --abort-on-container-exit --exit-code-from cypress mysql phpmyadmin flounder cypress | tee cypress-test-ui.log | grep "cypress_1"

sleep 1m
docker-compose down