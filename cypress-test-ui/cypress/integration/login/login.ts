/// <reference types="cypress" />
context('Call Flounder URL', () => {
    it('Check-Flounder-Server', () => {
        cy.wait(1000);
        cy.visit('');
        cy.get("#profile").should('be.visible');
        cy.screenshot('flounder_page');
        cy.get("#contact").click();
        cy.screenshot('contact_page');
    });
    it('Login-with-admin-user', () => {
        cy.wait(1000);
        cy.visit("flounder/?p=login");
        cy.url().should('include', '/?p=login');
        cy.get("#email").should('be.visible').type('admin@admin.com', {force: true}).should('have.value', 'admin@admin.com');
        cy.screenshot('admin_user');
        cy.get("#password").should('be.visible').type('admin', {force: true}).should('have.value', 'admin');
        cy.screenshot('admin_password');
        cy.get("#login_button").click();
        cy.screenshot('after_login_page');
    });
});
