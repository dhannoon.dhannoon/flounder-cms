/// <reference types="cypress" />
context('Register_new_user', () => {
    it('register_with_student1', () => {
        cy.wait(1000);
        cy.visit("flounder/?p=register");
        cy.url().should('include', '/?p=register');
        cy.get("#firstname").should('be.visible').type('student1_firstname', {force: true}).should('have.value', 'student1_firstname');
        cy.get("#lastname").should('be.visible').type('student1_lastname', {force: true}).should('have.value', 'student1_lastname');
        cy.get("#email").should('be.visible').type('student1@student.com', {force: true}).should('have.value', 'student1@student.com');
        cy.get("#date_of_birth").should('be.visible').type('1997-09-23', {force: true}).should('have.value', '1997-09-23');
        cy.get("#password").should('be.visible').type('student1', {force: true}).should('have.value', 'student1');
        cy.get("#confirmpassword").should('be.visible').type('student1', {force: true}).should('have.value', 'student1');
        cy.screenshot('register_data');
        cy.wait(100);
        cy.get("#submit_register").click();
        cy.wait(100);
        cy.screenshot('student1_data_after_register');
        cy.wait(100);
    });
});
