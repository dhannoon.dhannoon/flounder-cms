/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// module.exports = (on, config) => {
//   // `on` is used to hook into various events Cypress emits
//   // `config` is the resolved Cypress config
// }

module.exports = (on, config) => {
  on('before:browser:launch', (browser = {name}, launchOptions) => {
    //if (browser.family === 'chromium' && browser.name === 'chrome')
    //if (browser.name === 'electron' && browser.isHeadless || browser.headed) 
    if (browser.name === 'chrome') {
      //launchOptions.args.width = 1920;
      //launchOptions.args.height = 1196;
      launchOptions.args.push('--disable-dev-shm-usage');
      launchOptions.args.push('--window-size=1920,1196');
      //launchOptions.args.push('--start-fullscreen')
      launchOptions.preferences.width = 1920;
      launchOptions.preferences.height = 1196;
      launchOptions.preferences.fullscreen = true
      return launchOptions
    }
  })
}

