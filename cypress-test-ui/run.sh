#!/bin/sh

script=$0
cd $(dirname $script)
if [ -z "$FLOUNDER_URL" ]; then
    if [ -z "$HOST_DOCKER_INTERNAL" ]; then 
        if [ -f /sbin/ip ]; then
            export HOST_DOCKER_INTERNAL=$(/sbin/ip route|awk '/default/ { print $3 }')
        else 
            export HOST_DOCKER_INTERNAL=host.docker.internal
        fi
    fi
    FLOUNDER_URL=http://$HOST_DOCKER_INTERNAL:80/
fi

export FLOUNDER_URL
pwd
ls -l
echo "FLOUNDER_URL: ${FLOUNDER_URL}";
export CYPRESS_BASE_URL=${FLOUNDER_URL}
echo "CYPRESS_BASE_URL: ${CYPRESS_BASE_URL}";

#curl --silent --retry 100 --retry-connrefused --retry-delay 10 $FLOUNDER_URL/ctl/ok || exit 1

# export PHPMYADMIN_URL=http://$HOST_DOCKER_INTERNAL:8080

npm install
npm run wait-on -- $FLOUNDER_URL
echo "Calling cypress"
pwd
HAS_ERRORS=0

cypress -v
npm run cypress:run
if [ $? -ne 0 ]; then
    echo "Cypress tests did not pass"
    HAS_ERRORS=1
    else
    echo "Cypress tests passed"
fi


echo "Starting creating reports"
npm run posttest

if [ $? -ne 0 ]; then
    echo "Creating reports failed"
    HAS_ERRORS=1
    else
    echo "Reports created"
fi
# It's checking the return value ($?) or the retun valus of HAS_ERRORS not equal (-ne) to 0 then will exit code1 ci and if it is equal to 0 then will pass to the echo
if [ $HAS_ERRORS -ne 0 ]; then
    echo "There were errors"
    exit 1
    else
    echo "Everything is passed!"
fi
